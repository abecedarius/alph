			   Evolution essay

This is an "active essay" demonstrating some ideas about evolution,
originally expressed by Richard Dawkins in his book _The Blind
Watchmaker_.  Here we're going to recapitulate his argument but with
the processes described set in motion on the computer, as can't be
done on the printed page.  (This was inspired by Ted Kaehler's active
essay in Smalltalk on the same subject.)

Many people feel that evolution by natural selection can't account for
the exquisitely complicated life we see around us, because natural
selection is powered by chance, while complicated life is too
improbable to arise by chance.  It's true that mere randomness cannot
produce complexity, and we'll see that truth in the demo below; but
natural selection isn't *just* chance.  So let's try to generate a
complex structure by random chance, then see what else is needed.

Our target is a string of letters, and we'll consider it more
`organized' or `complex' the closer it comes to "METHINKS IT IS LIKE A
WEASEL":

:goal "METHINKS IT IS LIKE A WEASEL" ;

We'll name our `mere chance' process Drifter, and our later, better
process Evolver.  Here's how Drifter works: we pick a random element
of the alphabet and store it in a random position in the buffer.

:alphabet " ABCDEFGHIJKLMNOPQRSTUVWXYZ" ;
:goal-length (goal strlen constant)
:mutate {genome}
	random 27 umod  alphabet + c@  
	random goal-length umod genome + c! ;

:drifter  (here constant  goal-length allot  0 ,)
(drifter read-line  drifter mutate  drifter type)
|UFMHALBPSD KETPGPPGXFVSFJLZY

Hit M-i to see it working.  The code blocks should pick up '|' lines.

So, watching this, we can see that the fitness drifts around 1/27 of
the maximum fitness, and is horribly unlikely to get close to the
maximum -- getting the maximum would be like rolling a die 28 times in
a row and calling each result in advance.  (Only worse, since a die
only has 6 sides, compared to 27 `sides' here -- 26 letters plus the
space.)  Even a nation of gamblers couldn't expect that much luck in
the lifetime of the universe.

So let's try to make something better -- passing only mutations that
don't lower the fitness.  We define fitness as closeness to the goal:

:closeness-to-goal {genome i count}
	goal i + c@ 0= (if) count ; (then)
        genome i + c@  goal i + c@ = (if) 
		genome i 1+ count 1+ closeness-to-goal ;
	(then)  genome i 1+ count    closeness-to-goal ;
:fitness  0 0 closeness-to-goal ;

And here we have the result:

:parent  (here constant  goal-length 2+ allot)
:child   (here constant  goal-length 2+ allot)
:fitter   child fitness  parent fitness < (if)  parent ;  (then) child ;
(parent read-line  child parent strcpy  child mutate  fitter type)
|XXXXXXXXXXXXXXXXXXXXXXXXXXXX

You can hold down M-i and watch as Evolver's fitness tends to maximum
within around a thousand steps.  This shows that a process with random
mutations can produce extraordinarily improbable complex results if
improvements are *cumulative*.  The problem with Drifter is that any
improvements in one generation are forgotten in the next.

This can't be considered a realistic model of evolution; most
obviously, it has one particular goal wired in from the start, where
life has no goal.  All we've done here is refute the creationists'
claim that random mutations can't produce improbable results in a
reasonable time; they can, if the changes can accumulate.  This
creationist argument would make sense if biologists were saying that,
for example, one day an ape gave birth to a human, fully formed.  But
nobody says that; we must have had a long lineage shading
imperceptibly from near-apes to near-humans, with each change along
the way being reasonably likely on its own.


Some things to try: bear n offspring, selecting just one per
generation, with fitness-proportional selection.  Vary the mutation
rate.  Look for the error catastrophe.
