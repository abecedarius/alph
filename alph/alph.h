#ifndef ALPH_H
#define ALPH_H

#include "../tusl/tusl.h"

void alph_setup_tusl (ts_VM *vm);
void alph_setup (int argc, char **argv);
void alph_interact (void);
void alph_teardown (void);

#endif
