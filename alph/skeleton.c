/* Copyright 2005 by Darius Bacon; see the file COPYING. */

#include "../doe/doe.h"
#include "../tusl/tusl.h"
#include "../alph/alph.h"

#include <stdio.h>
#include <string.h>

static ts_VM *vm;

#define INSIST(e) ((e) ? (void)0 : insist (__FILE__, __LINE__, #e))

static void
insist (const char *file, int line, const char *expression)
{
  ts_error (vm, "Assertion failed (%s:%d): %s", file, line, expression);
}

static void
word (ts_VM *vm, ts_Word *pw) 
{
  ts_INPUT_0 (vm);
  ts_OUTPUT_0 ();
}

static void
install_my_words (ts_VM *vm)
{
#if 0
  ts_install (vm, "word",     word, 0);
  ts_install (vm, "cfunc",    ts_run_void_0, (int) cfunc);
#endif
}

int
main (int argc, char **argv)
{
  if (1 < argc && 0 == strcmp ("--help", argv[1]))
    {
      printf ("Usage: %s {-e command}* filename?\n", argv[0]);
      return 0;
    }

  vm = ts_vm_make ();
  ts_install_standard_words (vm);
#if 0
  ts_install_unsafe_words (vm);
#endif

  install_my_words (vm);

  alph_setup_tusl (vm);
  ts_load (vm, "tusl/tuslrc.ts");
  alph_setup (argc, argv);
  alph_interact ();
  alph_teardown ();

  ts_vm_unmake (vm);

  return 0;
}
