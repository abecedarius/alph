/* Copyright 2004-2005 by Darius Bacon; see the file COPYING. */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "alph.h"
#include "../doe/doe.h"

static Char_set mode_chars, nonblank, nonwhite, newline_or_block;


/* Regions */

typedef struct Region *Region;
struct Region {
  Mark lo;
  Mark hi;
};
/* Inv: mark_text(lo) == mark_text(hi) && mark_coord(lo) <= mark_coord(hi) */
/* Inv: something about the directions of the marks, too? */

static Region
region_make (Mark lo, Mark hi)
{
  Region r = allot (sizeof *r);
  r->lo = lo;
  r->hi = hi;
  return r;
}

static void
region_unmake (Region r)
{
  mark_unmake (r->hi);
  mark_unmake (r->lo);
  unallot (r);
}

static Region
region_build (Text t, Coord lo, Coord hi, Direction d)
{
  direction_check (d);
  return region_make (mark_make (t, lo, backward),
		      mark_make (t, hi, d));
}

static Mark
mark_copy (Mark m)
{
  mark_check (m, mark_text (m));
  return mark_make (mark_text (m), mark_coord (m), mark_direction (m));
}

static Region
region_copy (Region r)
{
  return region_make (mark_copy (r->lo), mark_copy (r->hi));
}

static void
region_replace (Region r, const Char *string, int length)
{
  text_replace (mark_text (r->lo), mark_coord (r->lo), 
		mark_coord (r->hi) - mark_coord (r->lo), 
		string, length);
}


/* I/O to and from the buffer */

/* A ts_Streamer that reads from a Region. */
static int
read_from_region (ts_VM *vm)
{
  ts_Stream *input = &vm->input;
  Region r = (Region) input->data;
  Text t = mark_text (r->lo);
  Coord lo = mark_coord (r->lo);
  Coord hi = mark_coord (r->hi);
  int n = min (sizeof input->buffer, hi - lo);
  text_get (t, lo, n, input->buffer);
  mark_unmake (r->lo);
  r->lo = mark_make (t, lo + n, backward);
  return n;
}

/* Hack alert! */
static boolean starting_fresh_line = yes;
static char output_block_char = '|';

static void
fresh_line (ts_VM *vm)
{
  ts_flush_output (vm);
  if (!starting_fresh_line)
    ts_put_char (vm, '\n');	/* N.B. automatically flushed */
}

/* A ts_Streamer that writes to a Mark. */
static int
write_to_mark (ts_VM *vm)
{
  ts_Stream *output = &vm->output;
  Mark m = (Mark) output->data;
  Text t = mark_text (m);
  Char *p = output->buffer;
  Char *q = output->ptr;

#if 0
  text_replace (t, mark_coord (m), 0, "[flush]", strlen ("[flush]"));
#endif

  if (p == q)
    return 0;

  /* Prefix '|' or '%' to each output line. */
  if (starting_fresh_line)
    text_replace (t, mark_coord (m), 0, &output_block_char, 1);
  starting_fresh_line = (q[-1] == '\n');

  /* You might think we should check for newlines in p..q, but 
     instead we expect line-at-a-time flushing from Tusl. */
  text_replace (t, mark_coord (m), 0, p, q - p);
  return q - p;
}

/* Set vm's input to come from r. */
static void
set_input_region (ts_VM *vm, Region r, const char *opt_regionname)
{
  ts_set_stream (&vm->input, read_from_region, (void *) r, opt_regionname);
}

/* Set vm's output to go to r. */
static void
set_output_mark (ts_VM *vm, Mark m, const char *opt_name)
{
  ts_set_stream (&vm->output, write_to_mark, (void *) m, opt_name);
}

static tsint *absorb_pdatum;
static boolean starting_input_line;

static void
fix_absorb (Region io)
{
  *absorb_pdatum = (tsint) io;
  starting_input_line = yes;
}

/* XXX change this to go through vm's I/O streams interface.
   Except we're already using that to read the *code* block. 
   The vm needs separate input streams for code and data, I suppose.
   Or else it should first load code into a 'TIB' or something before
   interpreting it. */
static void
do_absorb (ts_VM *vm, ts_Word *pw)
{
  ts_INPUT_0 (vm);
  {
    Region r = (Region) pw->datum;
    int result = -1;
    Coord hi = mark_coord (r->hi);
  again: 
    {
      Coord lo = mark_coord (r->lo);
      if (lo < hi)
	{
	  Text t = mark_text (r->lo);
	  Char c;
	  boolean sil = starting_input_line;
	  starting_input_line = no;

	  text_get (t, lo, 1, &c);
	  mark_unmake (r->lo);
	  r->lo = mark_make (t, lo + 1, backward);
	  if (sil)		/* Skip initial '|' or '%' */
	    goto again;
	  result = c;
	  starting_input_line = ('\n' == c);
	}
    }
    ts_OUTPUT_1 (result);
  }
}


/* Parsing the text for code and I/O blocks */

static Coord
end_of_line_or_block (Text t, Coord p)
{
  Coord at = text_find_char_set (t, p, forward, newline_or_block);
  return at == nowhere ? text_end (t) : at;
}

static Coord 
find_text_block_end (Text t, Coord p)
{
  while (p < text_end (t))
    {
      Coord q = text_find_char_set (t, p, forward, nonblank);
      if (nowhere == q)
	return text_end (t);
      {
	Char c;
	text_get (t, q, 1, &c);
	if ('\n' == c)
	  break;
	p = text_end_of_line (t, q) + 1;
      }
    }
  return p;
}

static Coord 
find_code_block_end (Text t, Coord p)
{
  /* ^[ \t\r]*[|\n] */
  while (p < text_end (t))
    {
      Coord q = text_find_char_set (t, p, forward, nonblank);
      if (nowhere == q)
	return text_end (t);
      {
	Char c;
	text_get (t, q, 1, &c);
	if ('\n' == c || '|' == c || '%' == c)
	  break;
	p = end_of_line_or_block (t, q);
	text_get (t, p, 1, &c);
	if ('|' == c || '%' == c)
	  break;
	++p;
      }
    }
  return p;
}

static Region
find_next_code_block (Text t, Coord p)
{
  /*
A block is bounded on either side by a blank line or an end of the Text.
Or else by an I/O area (a block with leading '|' characters).
A code block is a block starting with a mode_char.  
  */
  while (p < text_end (t))
    {
      Coord q = text_find_char_set (t, p, forward, nonwhite);
      if (nowhere == q)
	break;
      {
	Char c;
	text_get (t, q, 1, &c);
	if (char_set_has (mode_chars, c))
	  {
	    Coord r = find_code_block_end (t, q + 1);
	    return region_build (t, q, r, backward);
	  }
	else
	  p = find_text_block_end (t, q + 1);
      }
    }
  return NULL;
}

static Region
find_io_block (Text t, Coord p)
{
  /* (\s*|[^\n]*\n)* */
  Coord q = p;
  while (q < text_end (t))
    {
      Coord r = text_find_char_set (t, q, forward, nonblank);
      if (nowhere == r)
	{
	  q = text_end (t);
	  break;
	}
      else
	{
	  Char c;
	  text_get (t, r, 1, &c);
	  if ('\n' == c || ('|' != c && '%' != c))
	    break;
	  q = text_end_of_line (t, r) + 1;
	}
    }
  return region_build (t, p, q, backward);
}


/* Evaluating a buffer */

static ts_VM *vm;

static void
complain (const char *complaint)
{
  char obc = output_block_char;

  fresh_line (vm);
  output_block_char = '%';
  ts_put_string (vm, complaint, strlen (complaint));
  fresh_line (vm);

  output_block_char = obc;
}

/* Evaluate the code region, using the io region for both input and output,
   and return the coordinate after the output block. */
static Coord
eval_region (Region code, Region io)
{
  Region input = region_copy (io);
  Mark output = 
    mark_make (mark_text (io->hi), mark_coord (io->hi), forward);

  set_input_region (vm, code, NULL);
  set_output_mark (vm, output, NULL);
  fix_absorb (io);
  starting_fresh_line = yes;
  output_block_char = '|';
  { 
    ts_TRY (vm, frame)
      {
	ts_loading_loop (vm);
	ts_POP_TRY (vm, frame);
      }
    ts_EXCEPT (vm, frame)
      {
	/* TODO: restore the stack or clear it or something */
	complain (frame.complaint);
      }
  }
  fresh_line (vm);

  region_replace (input, "", 0);
  
  {
    Coord after = mark_coord (output);
    mark_unmake (output);
    region_unmake (input);
    return after;
  }
}

static void
eval_text (Text t)
{
  Coord p = 0;
  for (;;) 
    {
      Region code = find_next_code_block (t, p);
      if (NULL == code)
	break;
      {
	Region io = find_io_block (t, mark_coord (code->hi));
	p = eval_region (code, io);
	region_unmake (io);
	region_unmake (code);
      }
    }
}

#include <curses.h>

static int
check_for_interrupt (ts_VM *vm, ts_Word *pw)
{
  /* We only check once per countdown period because getch() turns out to
     be ridiculously expensive, at least on Cygwin. */
  static int countdown = 0;
  if (--countdown < 0)
    {
      int key;
      countdown = 1000;
      key = getch ();
      if (ERR != key)
	{
	  /* TODO: buffer this key ourselves until the whole
	     evaluation is done or aborted -- otherwise we're looking
	     at the same keypress every time through after the first
	     one, and we never see a subsequent C-g. */
	  ungetch (key);
	  if (CTRL ('G') == key)
	    ts_error (vm, "Aborted");
	}
    }
  return no;
}

void
tusl_eval_buffer (Buffer b)
{
  int here = vm->here;
  int there = vm->there;
  int where = vm->where;
  int sp = vm->sp;
  ts_Stream input = vm->input;
  ts_Stream output = vm->output;
  ts_CTraceFn *ctf = vm->colon_tracer;
  void *ctfd = vm->colon_tracer_data;

#if 1
  nodelay (stdscr, TRUE);
  vm->colon_tracer = check_for_interrupt;
  vm->colon_tracer_data = NULL;
#endif

  /* Ensure consistent initialization from one evaluation to the next */
  memset (vm->data + vm->here, 0, vm->there - vm->here);

  eval_text (buffer_text (b));

  nodelay (stdscr, FALSE);
  vm->colon_tracer_data = ctfd;
  vm->colon_tracer = ctf;

  vm->output = output;
  vm->input = input;
  vm->sp = sp;
  vm->where = where;
  vm->there = there;
  vm->here = here;
}


/* Setup */

/* Like the default error handler, but without the source-location
   prefix, as that usually just clutters up the screen under Doe. */
static const char *
bare_error (ts_VM *vm, const char *message, va_list args)
{
  /* FIXME code duplication */
  /* We place the complaint inside vm's data space so it can be accessed
     from within the vm without importing unsafe operations. */
  char *buffer = vm->data + vm->here, *scan = buffer;
  int size = vm->there - vm->here;
  if (size < 8)
    return vm->data + 1; /* Holds last-resort error message */
  vsnprintf (scan, size, message, args);
  return buffer;
}

DEFCMD(eval_buffer)          { tusl_eval_buffer (b); }

static Binding my_keyrib[] = {
  { META('i'),            0,         cmd_eval_buffer },
  { any_key,              0,         cmd_keep_looking }
};

void
alph_setup_tusl (ts_VM *the_vm)
{
  mode_chars = char_set_from_string (":()\\");
  newline_or_block = char_set_from_string ("\n|%");
  nonblank = char_set_not (char_set_from_string (" \t\r\f"));
  nonwhite = char_set_not (char_set_from_string (" \t\r\n\f"));

  vm = the_vm;

  absorb_pdatum = &vm->words[vm->where].datum;
  ts_install (vm, "absorb", do_absorb, 0);

  vm->error = bare_error;
  vm->error_data = NULL;
}


static void
run_argv_commands (int argc, char **argv)
{
  int i = 1;
  while (i < argc)
    if (i + 1 < argc && 0 == strcmp ("-e", argv[i]))
      {
	ts_load_string (vm, argv[i + 1]);
	i += 2;
      }
    else
      ++i;			/* Leave this one for visit_argv_files() */
}

static void
visit_argv_files (int argc, char **argv)
{
  int i = 1;
  while (i < argc)
    if (i + 1 < argc && 0 == strcmp ("-e", argv[i]))
      i += 2;
    else
      {
	doe_visit (argv[i]);
	++i;
      }
  {
    Name_table nt = editor_name_table (the_editor);
    if (2 < name_table_count (nt))
      name_table_remove (nt, "=scratch=");
  }
}

void
alph_setup (int argc, char **argv)
{
  run_argv_commands (argc, argv) ;
  doe_setup ();
  {
    Key_map map = editor_fundamental_keymap (the_editor);
    editor_set_fundamental_keymap (the_editor,
				   key_map_extend (map, my_keyrib));
  }
  visit_argv_files (argc, argv) ;
}

void
alph_teardown (void)
{
  Key_map map = editor_fundamental_keymap (the_editor);    
  doe_teardown ();
  unallot (map); /* TODO: make this something like key_map_unextend */
}

void
alph_interact (void)
{
  doe_interact ();
}
