Welcome to Alph!  An Alph document is *active*: when you hit M-i it will
do something depending on what's in it.  The traditional first thing for
any programming system to do is to type out "Hello, world!":

("Hello, world!" type
|Hello, world!

In these two lines the first, starting with "(", is a command, while
the second starting with "|" is its result.  Try changing the first
line's "Hello" to "Goodbye" and hitting M-i; the second line should
change then, too.

[something here about how this works: 
 - paragraph starting with (
 - "Hello world" is a string literal
 - "type" is a command
 - object-verb is like selecting an icon and then the action
And give an example of erroneous code]

Just saying hello isn't very exciting, so let's sing a song, "The 12
Days of Christmas".  It starts out:

("On the first day of Christmas, my true love gave to me" type cr
("A partridge in a pear tree." type cr cr
("On the second day of Christmas, my true love gave to me" type cr
("Two turtle doves," type cr
("And a partridge in a pear tree." type
|On the first day of Christmas, my true love gave to me
|A partridge in a pear tree.
|
|On the second day of Christmas, my true love gave to me
|Two turtle doves,
|And a partridge in a pear tree.

The "cr" starts a new line in the result.  Already this is looking
repetitive; isn't the point of computers to spare us this gruntwork?
So we define some new words that factor out the redundancy.  For a
start, the first line of each verse is almost the same:

:intro	" day of Christmas, my true love gave to me" type cr ;
("On the first" type intro 
("On the second" type intro 
|On the first day of Christmas, my true love gave to me
|On the second day of Christmas, my true love gave to me

The ":intro" line defines the word "intro" to do the commands that 
follow it, up to the ";" at the end.  Now we can use this new command
the same way as the built-in ones like "type" and "cr":

:gift1	"A partridge in a pear tree." type cr ;
:day1	"On the first" type intro  gift1 ;
(day1
|On the first day of Christmas, my true love gave to me
|A partridge in a pear tree.

:gift2	"Two turtle doves," type cr
	"And a partridge in a pear tree." type cr ;
:day2	"On the second" type intro  gift2 ;
(day2
|On the second day of Christmas, my true love gave to me
|Two turtle doves,
|And a partridge in a pear tree.

:gift3	"Three French hens," type cr  gift2 ;
:day3	"On the third" type intro  gift3 ;
(day3
|On the third day of Christmas, my true love gave to me
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.

:gift4	"Four calling birds,"       type cr  gift3 ;
:gift5	"Five gold rings,"          type cr  gift4 ;
:gift6	"Six geese a-laying,"       type cr  gift5 ;
:gift7	"Seven swans a-swimming,"   type cr  gift6 ;
:gift8	"Eight maids a-milking,"    type cr  gift7 ;
:gift9	"Nine ladies dancing,"      type cr  gift8 ;
:gift10	"Ten lords a-leaping,"      type cr  gift9 ;
:gift11	"Eleven pipers piping,"     type cr  gift10 ;
:gift12	"Twelve drummers drumming," type cr  gift11 ;

:day4	"On the fourth"   type intro gift4 ;
:day5	"On the fifth"    type intro gift5 ;
:day6	"On the sixth"    type intro gift6 ;
:day7	"On the seventh"  type intro gift7 ;
:day8	"On the eighth"   type intro gift8 ;
:day9	"On the ninth"    type intro gift9 ;
:day10	"On the tenth"    type intro gift10 ;
:day11	"On the eleventh" type intro gift11 ;
:day12	"On the twelfth"  type intro gift12 ;

:sing	day1 cr  day2 cr  day3 cr  day4 cr  day5 cr  day6 cr
	day7 cr  day8 cr  day9 cr  day10 cr  day11 cr  day12 ;

(sing
|On the first day of Christmas, my true love gave to me
|A partridge in a pear tree.
|
|On the second day of Christmas, my true love gave to me
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the third day of Christmas, my true love gave to me
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the fourth day of Christmas, my true love gave to me
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the fifth day of Christmas, my true love gave to me
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the sixth day of Christmas, my true love gave to me
|Six geese a-laying,
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the seventh day of Christmas, my true love gave to me
|Seven swans a-swimming,
|Six geese a-laying,
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the eighth day of Christmas, my true love gave to me
|Eight maids a-milking,
|Seven swans a-swimming,
|Six geese a-laying,
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the ninth day of Christmas, my true love gave to me
|Nine ladies dancing,
|Eight maids a-milking,
|Seven swans a-swimming,
|Six geese a-laying,
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the tenth day of Christmas, my true love gave to me
|Ten lords a-leaping,
|Nine ladies dancing,
|Eight maids a-milking,
|Seven swans a-swimming,
|Six geese a-laying,
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the eleventh day of Christmas, my true love gave to me
|Eleven pipers piping,
|Ten lords a-leaping,
|Nine ladies dancing,
|Eight maids a-milking,
|Seven swans a-swimming,
|Six geese a-laying,
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.
|
|On the twelfth day of Christmas, my true love gave to me
|Twelve drummers drumming,
|Eleven pipers piping,
|Ten lords a-leaping,
|Nine ladies dancing,
|Eight maids a-milking,
|Seven swans a-swimming,
|Six geese a-laying,
|Five gold rings,
|Four calling birds,
|Three French hens,
|Two turtle doves,
|And a partridge in a pear tree.

The same sort of program shows up in real applications like IC design
and L-systems.

What if we want to see just one verse at a time?

:verse1		"verse2"  type cr  day1 ;
:verse2		"verse3"  type cr  day2 ;
:verse3		"verse4"  type cr  day3 ;
:verse4		"verse5"  type cr  day4 ;
:verse5		"verse6"  type cr  day5 ;
:verse6		"verse7"  type cr  day6 ;
:verse7		"verse8"  type cr  day7 ;
:verse8		"verse9"  type cr  day8 ;
:verse9		"verse10" type cr  day9 ;
:verse10	"verse11" type cr  day10 ;
:verse11	"verse12" type cr  day11 ;
:verse12	"verse1"  type cr  day12 ;

(here read-line here find drop execute      \XXX too much magic
|verse2
|On the first day of Christmas, my true love gave to me
|A partridge in a pear tree.
