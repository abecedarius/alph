/* If you use a type with a larger word size here (e.g. 64 bits), code
   will break.  Also, on a 16-bit machine you'll need to change
   og_i32 to be a long int, etc. */

typedef          int  og_i32;	/* 32-bit, signed */
typedef unsigned int  og_u32;	/* 32-bit, unsigned */
typedef   signed char og_i8;	/*  8-bit, signed */
typedef unsigned char og_u8;	/*  8-bit, unsigned */

/* printf formats */
#define og_i32_fmt "%d"
#define og_u32_fmt "%u"
#define og_x32_fmt "%x"


/* The effect of >> on a negative number is not specified by the C
   standard.  We need it to round to -infinity, so we encapsulate that in
   SRA: */

#define og_port_SRA(x, y)    ( (x) >> (y) )


/* The direction of rounding in division of signed integers is
   implementation-specified.  These should round towards 0: */
/* FIXME these aren't used yet */

#define og_IDIV0(x, y)   ( (x) / (y) )
#define og_IMOD0(x, y)   ( (x) % (y) )

/* There are other unspecified behaviors like arithmetic overflow, but
   I haven't got around to wrappers for them yet... */


/* FIXME this isn't used yet */
#ifdef WORDS_BIGENDIAN
enum { og_endianness = 0 };
#else
enum { og_endianness = 3 };
#endif
