CLEAN  += og/main.o og/og.o og/runog

OG_H   := og/og.h og/porting.h

og/og.o: $(OG_H) og/og.c

og/main.o: $(OG_H) $(TUSL_H) $(DOE_H) $(ALPH_H) og/main.c

og/runog: $(OFILES) og/main.o og/og.o
	$(CC) $(CFLAGS) -o og/runog $^ -lncurses
