#ifndef OG_H
#define OG_H

#include "porting.h"


typedef struct og_VM og_VM;

struct og_VM {
  og_u32 pc, npc;
  og_i32 reg[32];
  og_u32 hi, lo;
  double freg[32];
  og_u8 *space;
  int space_size;
  int read_only_size;
  int timeslice;
  void *clist;			/* stub */
};


og_u8  og_fetchb (og_VM *vm, og_u32 addr);
og_i32 og_fetch  (og_VM *vm, og_u32 addr);
void   og_storeb (og_VM *vm, og_u32 addr, og_u8 value);
void   og_store  (og_VM *vm, og_u32 addr, og_i32 value);

void   og_set_reg (og_VM *vm, int dest, og_i32 value);

void   og_run (og_VM *vm);

#endif
