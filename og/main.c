/* Copyright 2005 by Darius Bacon; see the file COPYING. */

#include "og.h"
#include "../doe/doe.h"
#include "../tusl/tusl.h"
#include "../alph/alph.h"

#include <stdio.h>
#include <string.h>

enum { memory_size = 64*1024 };


static ts_VM *vm;

static og_VM the_vm;
static og_VM *og = &the_vm;

#define INSIST(e) ((e) ? (void)0 : insist (__FILE__, __LINE__, #e))

static void
insist (const char *file, int line, const char *expression)
{
  ts_error (vm, "Assertion failed (%s:%d): %s", file, line, expression);
}

static void
do_reset_og (void)
{
  int i;

  og->pc = 0;
  og->npc = og->pc + 4;
  memset (og->reg, 0, sizeof og->reg);
  og->hi = og->lo = 0;
  for (i = 0; i < 32; ++i)
    og->freg[i] = 0;
  memset (og->space, 0, og->space_size);
  og->timeslice = 0;
  og->clist = NULL;
}

static void
setup_og (void)
{
  og->space_size = memory_size;
  og->space = allot (memory_size);
  do_reset_og ();
}

static og_u32
do_get_slice (void)
{
  return og->timeslice;
}

static void
do_set_slice (og_u32 slice)
{
  og->timeslice = slice;
}

static int			/* XXX og_u32? */
do_get_pc (void)
{
  return og->pc;
}

static void
do_set_pc (og_u32 addr)
{
  INSIST (0 <= addr && addr < og->space_size);
  INSIST (0 == (3 & addr));
  og->pc = addr;
  og->npc = og->pc + 4;
}

static int
do_get_reg (int r)
{
  INSIST (0 <= r && r < 32);
  return og->reg[r];
}

static void
do_set_reg (og_i32 value, int r)
{
  INSIST (1 <= r && r < 32);
  og->reg[r] = value;
}

static og_i32
do_fetch (og_u32 addr)
{
  INSIST (0 <= addr && addr < og->space_size);
  INSIST (0 == (3 & addr));
  return og_fetch (og, addr);
}

static void
do_store (og_i32 value, og_u32 addr)
{
  INSIST (0 <= addr && addr < og->space_size);
  INSIST (0 == (3 & addr));
  og_store (og, addr, value);
}

static og_u8
do_fetchb (og_u32 addr)
{
  INSIST (0 <= addr && addr < og->space_size);
  return og_fetchb (og, addr);
}

static void
do_storeb (og_u8 value, og_u32 addr)
{
  INSIST (0 <= addr && addr < og->space_size);
  og_storeb (og, addr, value);
}

static void
do_go (void)
{
  og_run (og);
}

static void
install_og_words (ts_VM *vm)
{
  ts_install (vm, "vm-reset",     ts_run_void_0, (tsint) do_reset_og);

  ts_install (vm, "slice@",       ts_run_int_0,  (tsint) do_get_slice);
  ts_install (vm, "slice!",       ts_run_void_1, (tsint) do_set_slice);
  ts_install (vm, "pc@",          ts_run_int_0,  (tsint) do_get_pc);
  ts_install (vm, "pc!",          ts_run_void_1, (tsint) do_set_pc);
  ts_install (vm, "reg@",         ts_run_int_1,  (tsint) do_get_reg);
  ts_install (vm, "reg!",         ts_run_void_2, (tsint) do_set_reg);
  ts_install (vm, "vm@",          ts_run_int_1,  (tsint) do_fetch);
  ts_install (vm, "vm!",          ts_run_void_2, (tsint) do_store);
  ts_install (vm, "vmc@",         ts_run_int_1,  (tsint) do_fetchb);
  ts_install (vm, "vmc!",         ts_run_void_2, (tsint) do_storeb);

  ts_install (vm, "vm-go",        ts_run_void_0, (tsint) do_go);
}

void
debug_put (const char *format, ...)
{
  va_list args;
  va_start (args, format);
  {
    char buffer[1024];
    int n = vsnprintf (buffer, sizeof buffer, format, args);
    assert (0 <= n);
    if (sizeof buffer - 1 < n)
      n = sizeof buffer - 1;

    ts_put_string (vm, buffer, n);
    ts_flush_output (vm);
  }
  va_end (args);
}

int
main (int argc, char **argv)
{
  if (1 < argc && 0 == strcmp ("--help", argv[1]))
    {
      printf ("Usage: %s {-e command}* filename?\n", argv[0]);
      return 0;
    }

  vm = ts_vm_make ();
  ts_install_standard_words (vm);
  ts_install_unsafe_words (vm);

  setup_og ();
  install_og_words (vm);

  alph_setup_tusl (vm);
  ts_load (vm, "tusl/tuslrc.ts");
  alph_setup (argc, argv);
  alph_interact ();
  alph_teardown ();

  ts_vm_unmake (vm);

  return 0;
}
