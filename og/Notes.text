   Change this to be register-based, something like a simple RISC.
   (This puts temporaries outside the address space; along with
   read-only code segments this makes compilation practical.  Also we
   might need an instruction for flushing the notional code cache or
   something, similarly.)

   Define some table layout the C code is expecting describing 
   entry points it can call.
   One standard entry in the clist should describe stuff the VM code
   can link to, conversely.

   The standard image file format should be just a literal coredump
   plus an indication of how much space it needs after expanding
   itself.  It should have a standard entry point for expanding itself
   out of whatever compressed format it might be in.  Consider
   supporting streaming somehow (see notes on that for Idel).

   What about 64-bit stuff?
   32 64-bit ints   = 256 bytes
   32 80-bit floats = 320 bytes
   Minimum size of a VM is probably around 1kb + heap space...
   Should it also have an incoming queue for eventual sends?
   I wanted to make finer-grained objects supportable...

   Change to have two memory segments, making sharing possible.  But
   fancier memory maps than that are probably too expensive.  Instead,
   provide cheap capability invocations between separate VM instances.
   Maybe that'd require 3 or 4 segments to be usable, though, like:
     - code
     - 'static' data + heap + stack (kind of painful to keep all together)
     - shared segment for friends
     - input/output buffer for IPC
   Another sample use for a shared segment: 
     - frame buffer for libsdl

   And once we're supporting 4 segments, it's probably just as easy to
   support N.  The good old zero-one-infinity principle.  Oh, well.
   See if we can use some kind of hinting so memory translation can be
   coded inline with a single-entry TLB for any particular operation.
   (It might be a different TLB for each different kind of operation.)
   Don't bother in the initial implementation, of course.

   Maybe special address registers containing known-safe pointers, too --
   I think PowerPC has something vaguely similar.  Probably not worth the
   complexity, though.
