#include <stdio.h>

static int seed = 9973;

static int 
randnum (void)
{
  //  seed = ((seed * 32749) + 32649) % 32497;
  seed *= 32749;
  //printf ("%x\n", seed);
  seed += 32649;
  //printf ("%x\n", seed);
  seed %= 32497;
  return seed;
}

int
main ()
{
  printf ("%d\n", randnum ());
  printf ("%d\n", randnum ());
  printf ("%d\n", randnum ());
  printf ("%d\n", randnum ());
  return 0;
}
