#include "og.h"

og_u8
og_fetchb (og_VM *vm, og_u32 addr)	/* XXX unsigned address, right? */
{
  /* XXX check space_size range */
  return vm->space [addr];
}

og_i32
og_fetch (og_VM *vm, og_u32 addr)
{
  /* XXX check space_size range */
  return *(int *)(vm->space + addr);	/* XXX alignment, endianness */
}

void 
og_storeb (og_VM *vm, og_u32 addr, og_u8 value)
{
  /* XXX check read_only_size range */
  vm->space[addr] = value;
}

void 
og_store (og_VM *vm, og_u32 addr, og_i32 value)
{
  /* XXX check read_only_size range */
  *(og_i32 *)(vm->space + addr) = value;	/* XXX alignment, endianness */
}

void
og_set_reg (og_VM *vm, int dest, og_i32 value)
{
  vm->reg[dest] = value;
  vm->reg[0] = 0;
}

static og_u32
SRL (og_u32 a, og_u32 amount)
{
  return a >> amount;
}

static og_i32
SRA (og_i32 a, og_u32 amount)
{
  return og_port_SRA (a, amount);	/* ugh */
}

static void
do_syscall (og_VM *vm)
{

}

static og_u32 
insn_immu (og_u32 insn)
{
  return 0xffff & insn;
}

static og_i32 
insn_imms (og_u32 insn)
{
  og_i32 x = 0xffff & insn;
  return (x << 16) >> 16;	/* XXX or portable equivalent */
}

static void
mult (og_VM *vm, og_i32 m, og_i32 n)
{				/* XXX unportable */
  long long product = (long long)m * (long long)n;
  vm->lo = (og_i32)product;
  vm->hi = (og_i32)(product >> 32);
}

static void
multu (og_VM *vm, og_u32 m, og_u32 n)
{
  unsigned long long product = (unsigned long long)m * (unsigned long long)n;
  vm->lo = (og_u32)product;
  vm->hi = (og_u32)(product >> 32);
}

static void
advance_pc (og_VM *vm, og_i32 offset)
{
  vm->pc = vm->npc;
  vm->npc = vm->pc + offset;	/* XXX wrong for branches, I think */
}

static void
cond_branch (og_VM *vm, int branch, og_i32 offset)
{
  if (branch)
    advance_pc (vm, 4 + (offset << 2));
  else
    advance_pc (vm, 4);
}

static void
cond_branch_and_link (og_VM *vm, int branch, og_i32 offset)
{
  if (branch)
    {
      vm->reg[31] = vm->pc + 8;
      advance_pc (vm, 4 + (offset << 2));
    }
  else
    advance_pc (vm, 4);
}

enum {
  opc_3reg_op = 000, opc_addi    = 010,
  opc_bz_op   = 001, opc_addiu   = 011,
  opc_j       = 002, opc_slti    = 012,
  opc_jal     = 003, opc_sltiu   = 013,
  opc_beq     = 004, opc_andi    = 014,
  opc_bne     = 005, opc_ori     = 015,
  opc_bltz    = 006, opc_xori    = 016,
  opc_bgez    = 007, opc_lui     = 017,
  
  opc_lb      = 040, opc_sb      = 050,

  opc_lw      = 043, opc_sw      = 053
};

enum {
  opc3_sll    = 000, opc3_jr     = 010, opc3_mfhi   = 020, opc3_mult   = 030, 
                                                           opc3_multu  = 031, 
  opc3_srl    = 002,                    opc3_mflo   = 022, opc3_div    = 032, 
  opc3_sra    = 003,                                       opc3_divu   = 033, 
  opc3_sllv   = 004, opc3_syscall= 014,

  opc3_srlv   = 006,

  opc3_add    = 040,
  opc3_addu   = 041,
  opc3_sub    = 042, opc3_slt    = 052,
  opc3_subu   = 043, opc3_sltu   = 053,
  opc3_and    = 044,
  opc3_or     = 045,
  opc3_xor    = 046		/* TODO there's a NOR instruction too */
};

enum {
  /* FIXME why do we have this bltz/bgez as well as the above ones? */
  opc_bz_bltz   = 000, opc_bz_bltzal = 040,
  opc_bz_bgez   = 001, opc_bz_bgezal = 041
};

extern void debug_put (const char *string, ...); /* FIXME */

void 
og_run (og_VM *vm)
{
  for (;;)
    {
      og_u32 insn, rs, rt;
      if (vm->timeslice <= 0)
	return;
      vm->timeslice--;
#if 0
      printf ("\n**slices: %d  pc: %d  npc: %d**\n",
	      vm->timeslice, vm->pc, vm->npc);
      debug_put ("starting insn at %d\n", vm->pc);
#endif

      insn = og_fetch (vm, vm->pc);
      rs = 0x1f & (insn >> 21);
      rt = 0x1f & (insn >> 16);
      switch (insn >> 26)	/* op */
	{
	case opc_3reg_op:
	  {
	    og_u32 rd = 0x1f & (insn >> 11);
	    og_u32 shamt = 0x1f & (insn >> 6);
	    switch (0x3f & insn) /* funct */
	      {
	      case opc3_sll:
		if (0 != rs) goto illegal_insn;	/* XXX right? */
		og_set_reg (vm, rd, vm->reg[rt] << shamt);
		advance_pc (vm, 4);
		break;
	      case opc3_srl:
		if (0 != rs) goto illegal_insn;	/* XXX right? */
		og_set_reg (vm, rd, SRL (vm->reg[rt], shamt));
		advance_pc (vm, 4);
		break;
	      case opc3_sra:
		if (0 != rs) goto illegal_insn;	/* XXX right? */
		og_set_reg (vm, rd, SRA (vm->reg[rt], shamt));
		advance_pc (vm, 4);
		break;
	      case opc3_sllv:
		if (0 != shamt) goto illegal_insn;
		/* XXX use only low 5 bits of shift count? */
		og_set_reg (vm, rd, vm->reg[rt] << (0x1f & vm->reg[rs]));
		advance_pc (vm, 4);
		break;
	      case opc3_srlv:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, SRL (vm->reg[rt], vm->reg[rs]));
		advance_pc (vm, 4);
		break;
	      case opc3_jr:
		if (0 != (rt | rd | shamt)) goto illegal_insn;
		vm->pc = vm->npc;
		vm->npc = vm->reg[rs];
		break;
	      case opc3_syscall:
		if (0 != (rs | rt | rd | shamt)) goto illegal_insn;
		if (1)
		  return;	/* XXX stub */
		else
		  do_syscall (vm);
		advance_pc (vm, 4);
		break;
	      case opc3_mfhi:	
		if (0 != shamt) goto illegal_insn; /* XXX what about rs/rt? */
		og_set_reg (vm, rd, vm->hi);
		advance_pc (vm, 4);
		break;
	      case opc3_mflo:
		if (0 != shamt) goto illegal_insn; /* XXX what about rs/rt? */
		og_set_reg (vm, rd, vm->lo);
		advance_pc (vm, 4);
		break;
	      case opc3_mult:
		if (0 != shamt) goto illegal_insn; /* XXX what about rd? */
		mult (vm, (og_i32)vm->reg[rs], (og_i32)vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_multu:
		if (0 != shamt) goto illegal_insn; /* XXX what about rd? */
		multu (vm, (og_u32)vm->reg[rs], (og_u32)vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_div:
		if (0 != shamt) goto illegal_insn; /* XXX what about rd? */
		/* XXX define rounding */
		vm->lo = vm->reg[rs] / vm->reg[rt];
		vm->hi = vm->reg[rs] % vm->reg[rt];
		advance_pc (vm, 4);
		break;
	      case opc3_divu:
		if (0 != shamt) goto illegal_insn; /* XXX what about rd? */
		/* XXX define rounding */
		vm->lo = (og_u32)vm->reg[rs] / (og_u32)vm->reg[rt];
		vm->hi = (og_u32)vm->reg[rs] % (og_u32)vm->reg[rt];
		advance_pc (vm, 4);
		break;
	      case opc3_add:
		if (0 != shamt) goto illegal_insn;
		/* XXX overflow? */
		og_set_reg (vm, rd, vm->reg[rs] + vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_addu:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, vm->reg[rs] + vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_sub:
		if (0 != shamt) goto illegal_insn;
		/* XXX overflow? */
		og_set_reg (vm, rd, vm->reg[rs] - vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_subu:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, vm->reg[rs] - vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_and:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, vm->reg[rs] & vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_or:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, vm->reg[rs] | vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_xor:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, vm->reg[rs] ^ vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_slt:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, vm->reg[rs] < vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      case opc3_sltu:
		if (0 != shamt) goto illegal_insn;
		og_set_reg (vm, rd, 
			 (og_u32)vm->reg[rs] < (og_u32)vm->reg[rt]);
		advance_pc (vm, 4);
		break;
	      default:
		goto illegal_insn;
	      }
	    break;
	  }

	case opc_bz_op:
	  switch (rt)
	    {
	    case opc_bz_bgez:
	      cond_branch (vm, vm->reg[rs] >= 0, insn_imms (insn));
	      break;
	    case opc_bz_bgezal:
	      cond_branch_and_link (vm, vm->reg[rs] >= 0, insn_imms (insn));
	      break;
	    case opc_bz_bltz:
	      cond_branch (vm, vm->reg[rs] < 0, insn_imms (insn));
	      break;
	    case opc_bz_bltzal:
	      cond_branch_and_link (vm, vm->reg[rs] < 0, insn_imms (insn));
	      break;
	    default:
	      goto illegal_insn;
	    }
	  break;

	case opc_jal:
	  vm->reg[31] = vm->pc + 8;
	  /* fall through */
	case opc_j:
	  vm->pc = vm->npc;
	  vm->npc = 
	    (0xf0000000 & vm->pc) | 
	    ((0x3ffffff & insn) << 2);	/* target */
	  break;

	case opc_beq:
	  cond_branch (vm, vm->reg[rs] == vm->reg[rt], insn_imms (insn));
	  break;
	case opc_bne:
	  cond_branch (vm, vm->reg[rs] != vm->reg[rt], insn_imms (insn));
	  break;
	case opc_bltz:
	  if (0 != rt) goto illegal_insn;
	  cond_branch (vm, vm->reg[rs] < vm->reg[rt], insn_imms (insn));
	  break;
	case opc_bgez:
	  if (0 != rt) goto illegal_insn;
	  cond_branch (vm, vm->reg[rs] >= vm->reg[rt], insn_imms (insn));
	  break;

	case opc_addi:
	  /* XXX overflow? */
	  og_set_reg (vm, rt, vm->reg[rs] + insn_imms (insn));
	  advance_pc (vm, 4);
	  break;
	case opc_addiu:
	  /* XXX cast to og_u32 to avoid impl-defined overflow trap? */
	  og_set_reg (vm, rt, vm->reg[rs] + insn_imms (insn));
	  advance_pc (vm, 4);
	  break;
	case opc_slti:
	  og_set_reg (vm, rt, vm->reg[rs] < insn_imms (insn));
	  advance_pc (vm, 4);
	  break;
	case opc_sltiu:
	  og_set_reg (vm, rt, (og_u32)vm->reg[rs] < (og_u32)insn_imms (insn));
	  advance_pc (vm, 4);
	  break;
	case opc_andi:
	  og_set_reg (vm, rt, vm->reg[rs] & insn_immu (insn));
	  advance_pc (vm, 4);
	  break;
	case opc_ori:
	  og_set_reg (vm, rt, vm->reg[rs] | insn_immu (insn));
	  advance_pc (vm, 4);
	  break;
	case opc_xori:
	  og_set_reg (vm, rt, vm->reg[rs] ^ insn_immu (insn));
	  advance_pc (vm, 4);
	  break;
	case opc_lui:
	  if (0 != rs) goto illegal_insn;
	  og_set_reg (vm, rt, insn_immu (insn) << 16);
	  advance_pc (vm, 4);
	  break;

	case opc_lb:
	  /* XXX aren't loads supposed to be delayed? */
	  og_set_reg (vm, rt, og_fetchb (vm, vm->reg[rs] + insn_imms (insn)));
	  advance_pc (vm, 4);
	  break;
	case opc_lw:
	  og_set_reg (vm, rt, og_fetch (vm, vm->reg[rs] + insn_imms (insn)));
	  advance_pc (vm, 4);
	  break;
	case opc_sb:
	  og_storeb (vm, vm->reg[rs] + insn_imms (insn), 0xff & vm->reg[rt]);
	  advance_pc (vm, 4);
	  break;
	case opc_sw:
	  og_store (vm, vm->reg[rs] + insn_imms (insn), vm->reg[rt]);
	  advance_pc (vm, 4);
	  break;

	default:
	illegal_insn:
	  debug_put ("illegal insn\n");
	  return;		/* TODO trap to keeper */
	}
    }
}
