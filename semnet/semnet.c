/* Copyright 2005 by Darius Bacon; see the file COPYING. */

#include "../doe/doe.h"
#include "../tusl/tusl.h"
#include "../alph/alph.h"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

/* The triple store is a nul-terminated string holding 3-character
   triples one after the other. */
static char triples[1024] = "";

/* Clear the triple-store. */
static void
reset (void)
{
  triples[0] = '\0';
}

/* Add triple to the triple-store. */
static void
add_triple (const char *triple)
{
  assert (3 == strlen (triple));
  assert (strlen (triples) + strlen (triple) < sizeof triples);
  strcat (triples, triple);
}

/* The environment is a map from variables to their values, represented
   as a string of two-character pairs (holding the variable and its
   value) one after the other. */
static char environment[1024] = "";

/* Return true iff pattern matches datum in the current environment,
   extending the environment with a new binding if appropriate. */
static int
match1 (char datum, char pattern)
{
  if (isupper (pattern))
    {
      int i;
      for (i = 0; environment[i]; i += 2)
	if (pattern == environment[i])
	  return datum == environment[i+1];
      assert (i + 3 < sizeof environment);
      sprintf (environment + i, "%c%c", pattern, datum);
      return yes;
    }
  return datum == pattern;
}

/* Find all ways of matching query within the current environment,
   extending it according to the match and printing to vm each
   resulting environment. */
static void
do_query (ts_VM *vm, const char *query)
{
  int i, t = strlen (environment);
  if (!*query)
    {
      ts_put_string (vm, environment, t);
      ts_put_char (vm, '\n');
      return;
    }
  for (i = 0; triples[i]; i += 3)
    {
      environment[t] = '\0';
      if (match1 (triples[i+0], query[0]) &&
	  match1 (triples[i+1], query[1]) &&
	  match1 (triples[i+2], query[2]))
	do_query (vm, query + 3);
    }
}

/* Start a fresh query in an empty environment. */
static void
print_matches (ts_VM *vm, const char *query)
{
  environment[0] = '\0';
  do_query (vm, query);
}


/* Tusl wrappers for the above functions. */

static void
do_add_triple (ts_VM *vm, ts_Word *pw) 
{
  /* z is the input parameter, a pointer to a string relative to
     vm's internal address space. */
  ts_INPUT_1 (vm, z);
  /* The OUTPUT_0() is needed to adjust the stack pointer.  This
     scheme for expressing the input and output parameters of a
     C-coded word was originally designed for efficiency, and it
     worked out nicely for that; but Tusl evolved towards caring less
     about speed and we should probably try to make this more
     convenient. */
  ts_OUTPUT_0 ();
  /* ts_data_byte() converts the pointer to one in C's address space. */
  add_triple (ts_data_byte (vm, z));
}

static void
do_print_matches (ts_VM *vm, ts_Word *pw) 
{
  ts_INPUT_1 (vm, z);
  ts_OUTPUT_0 ();
  print_matches (vm, ts_data_byte (vm, z));
}

static void
install_my_words (ts_VM *vm)
{
  /* The 'reset' word doesn't need a new wrapper function because it 
     doesn't need to refer to the vm.  We just install it as a void
     function. */
  ts_install (vm, "reset",          ts_run_void_0, (tsint) reset);

  ts_install (vm, "add",            do_add_triple, 0);
  ts_install (vm, ".matches",       do_print_matches, 0);
}


/* Boilerplate to run the above stuff interactively in an editor. */
int
main (int argc, char **argv)
{
  if (1 < argc && 0 == strcmp ("--help", argv[1]))
    {
      printf ("Usage: %s {-e command}* filename?\n", argv[0]);
      return 0;
    }

  {
    ts_VM *vm = ts_vm_make ();
    ts_install_standard_words (vm);
    install_my_words (vm);

    alph_setup_tusl (vm);
    ts_load (vm, "tusl/tuslrc.ts");
    alph_setup (argc, argv);
    alph_interact ();
    alph_teardown ();
    
    ts_vm_unmake (vm);
  }
  return 0;
}
