CLEAN += semnet/semnet.o semnet/semnet

semnet/semnet.o: $(DOE_H) $(TUSL_H) semnet/semnet.c

semnet/semnet: semnet/semnet.o $(OFILES) 
	$(CC) $(CFLAGS) -o semnet/semnet $^ -lncurses
