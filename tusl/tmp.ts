\ (start-tracing)
:message (here constant 10 allot  0 message c!)

:string-blast   dup strlen screen-blast ;
:update         0 0 "hello" string-blast
                0 1 message string-blast
                0 0 screen-refresh ;

:react {key}    key message c!  0 message 1+ c!
                ;
:playing        update
                get-key {key}  key $q = (unless)
                  key react
                  playing ;

:?complain {plaint}
                plaint (when) plaint type cr ;
:play           screen-setup  'playing catch  screen-teardown  ?complain ;
\(start-tracing)
(play)
