/* Copyright 2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"

#include <stdio.h>
#include <string.h>

int
main (int argc, char **argv)
{
  if (1 < argc && 0 == strcmp ("--help", argv[1]))
    {
      printf ("Usage: %s {-e command}* filename?\n", argv[0]);
      return 0;
    }

  return 0;
}
