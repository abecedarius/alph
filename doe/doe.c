/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"
#include <signal.h>
#include <stdio.h>


static boolean
file_exists (const char *filename)
{
  FILE *f = fopen (filename, "r");
  boolean result = (f != NULL);	/* FIXME: check errno.  or use stat(). */
  if (f) fclose (f);
  return result;
}


static const Char *
visit_file (Name_table nt, const char *filename)
{
  Text t = text_make (0);
  Buffer b = buffer_make (t, style_make (8), filename);
  if (file_exists (filename))	/* FIXME: race condition */
    {
      text_insert_file (t, 0, filename);
      buffer_move_to (b, 0);
    }
  name_table_add (nt, filename, b);
  return filename;		/* FIXME: what if filename is already 
				          a buffer name? */
}


void
doe_visit (const char *filename)
{
  Name_table nt = editor_name_table (the_editor);
  Frame f = editor_frame (the_editor);
  const Char *b_name = visit_file (nt, filename);
  Buffer b = name_table_ref (nt, b_name);
	  
  /* FIXME: remove once we have multiple-buffer
     handling done right */
  frame_replace_active_display (f, b);

  if (!file_exists (filename))	/* FIXME: race condition */
    warn ("(New file)");
}


Editor the_editor;

static Terminal the_term;

void
doe_setup (void)
{
  derived_setup ();
  the_term = terminal_make ();
  the_editor = editor_make (the_term);
}

void
doe_teardown (void)
{
  editor_unmake (the_editor);
  terminal_unmake (the_term);
  derived_teardown ();
}

void
doe_interact (void)
{
  interactor_loop (editor_frame (the_editor));
}
