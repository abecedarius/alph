/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"

struct Editor {
  Name_table nt;
  Frame f;
  Text killed;
  Key_map keymap;
};


void
editor_check (Editor e)
{
  if (debug)
    {
      assert (e != NULL);
      name_table_check (e->nt);
      frame_check (e->f);
      text_check (e->killed);
      /* TODO: implement a check for keymaps */
    }
}


Editor
editor_make (Terminal term)
{
  Editor e = allot (sizeof *e);
  e->nt = name_table_make ();
  e->f = frame_make (term, e->nt);
  e->killed = text_make (0);
  e->keymap = doe_default_root_keymap;
  editor_check (e);
  return e;
}


void
editor_unmake (Editor e)
{
  editor_check (e);
  text_unmake (e->killed);
  frame_unmake (e->f);
  name_table_unmake (e->nt);
  unallot (e);
}


Name_table
editor_name_table (Editor e)
{
  editor_check (e);
  return e->nt;
}


Frame
editor_frame (Editor e)
{
  editor_check (e);
  return e->f;
}


Text
editor_killed (Editor e)
{
  editor_check (e);
  return e->killed;
}


Key_map
editor_fundamental_keymap (Editor e)
{
  editor_check (e);
  return e->keymap;  
}

void
editor_set_fundamental_keymap (Editor e, Key_map map)
{
  editor_check (e);
  e->keymap = map;
  editor_check (e);
}
