/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"

enum { bits = 32 };


const Char_set char_set_empty = { 0, 0, 0, 0 };


Char_set 
char_set_not (Char_set cs)
{
  Char_set r;
  r.u0 = ~cs.u0;
  r.u1 = ~cs.u1;
  r.u2 = ~cs.u2;
  r.u3 = ~cs.u3;
  return r;
}


Char_set 
char_set_and (Char_set cs1, Char_set cs2)
{
  Char_set r;
  r.u0 = cs1.u0 & cs2.u0;
  r.u1 = cs1.u1 & cs2.u1;
  r.u2 = cs1.u2 & cs2.u2;
  r.u3 = cs1.u3 & cs2.u3;
  return r;
}


Char_set 
char_set_or (Char_set cs1, Char_set cs2)
{
  Char_set r;
  r.u0 = cs1.u0 | cs2.u0;
  r.u1 = cs1.u1 | cs2.u1;
  r.u2 = cs1.u2 | cs2.u2;
  r.u3 = cs1.u3 | cs2.u3;
  return r;
}


boolean
char_set_has (Char_set cs, Char c)
{
  /* UNPORTABLE: assumes no padding between struct members. */
  const unsigned *p = &cs.u0;
  unsigned char uc = (unsigned char) c;
  return 0 != ((1u << (uc % bits)) & p[uc / bits]);
}


static unsigned
subrange (Char low, Char high, int w)
{
  unsigned r = 0, i;

  for (i = 0; i < bits; ++i)
    {
      int index = w * bits + i;
      if (low <= index && index < high)
	r |= (1 << i);
    }
  return r;
}


/* Pre: lo <= hi+1 */
Char_set
char_set_range (Char lo, Char hi)
{
  Char_set r;
  r.u0 = subrange (lo, hi+1, 0);
  r.u1 = subrange (lo, hi+1, 1);
  r.u2 = subrange (lo, hi+1, 2);
  r.u3 = subrange (lo, hi+1, 3);
  return r;
}
