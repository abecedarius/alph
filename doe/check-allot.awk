#!/bin/gawk -f

# Poor man's malloc debugger.  Finds memory leaks, double-freeing, etc.
# You should probably just use Valgrind instead, nowadays.
#
# Run this on the messages produced when you #define show_malloc in
# helpers.c.
#
# TODO: we could also track total allocations and max space used.


{ location = $1; action = $2; p = $3; }

action == "allot" { 
  size = $4;
  if (p in status && status[p] != "freed")
    complain("already allotted");
  loc[p] = location;
  status[p] = "allotted";
  block[p] = size;
  next;
}

action == "reallot" {
  size = $4;
  old = $5;
  if (old != "(nil)" && !(old in status && status[old] == "allotted"))
    complain("reallotted random pointer (" old ")");
  if (p in status && p != old && status[p] != "freed")
    complain("already allotted");
  status[old] = "freed";
  loc[p] = location;
  status[p] = "allotted";
  block[p] = size;
  next;
}

action == "free" {
  if (!(p in status))
    complain("freed random pointer");
  else
    {
      if (status[p] == "freed" && p != "(nil)")
	complain("already freed");
      loc[p] = location;
      status[p] = "freed";
    }
  next;
}

{ print "unparsable trace: " $0; }

END {
  action = "cleanup";
  for (p in status)
    if (status[p] != "freed")
      {
	location = loc[p];
	complain("leaking " block[p]);
      }
}

function complain(message)
{
  printf("%-20s %-7s %10s: %s\n", location, action, p, message);
}
