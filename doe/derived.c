/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"
#include <ctype.h>
#include <stdio.h>

/* Various necessary functions that don't depend on data-structure
   internals. */


static Char_set newline, blank_chars, blank_or_newline_chars, nonblank_chars, 
  word_chars, nonword_chars;

Char_set
char_set_from_string (const char *string)
{
  Char_set cs = char_set_empty;
  for (; *string; ++string)
    cs = char_set_or (cs, char_set_range (*string, *string));
  return cs;
}

void
derived_setup (void)
{
  newline = char_set_range ('\n', '\n');
  blank_chars = char_set_from_string (" \t\f\r");
  blank_or_newline_chars = char_set_or (blank_chars, newline);
  nonblank_chars = char_set_not (blank_chars);
  {
    Char_set digits = char_set_range ('0', '9');
    Char_set letters = char_set_or (char_set_range ('A', 'Z'),
				    char_set_range ('a', 'z'));

    word_chars = char_set_or (letters,
			      char_set_or (digits,
					   char_set_range ('\'', '\'')));
    nonword_chars = char_set_not (word_chars);
  }
}

void 
derived_teardown (void)
{
}


void
text_delete_span (Text t, Coord p, int length)
{
  /* TODO: wouldn't it be simpler overall if text_replace could
     take a negative length? */
  if (0 <= length)
    text_replace (t, p, length, "", 0);
  else
    text_replace (t, p + length, -length, "", 0);
}

void
text_copy (Text dest, Coord at, Text src, Coord p, int length)
{
  Char *chars = allot (length);
  text_get (src, p, length, chars); /* FIXME: get actual length */
  text_replace (dest, at, 0, chars, length);
  unallot (chars);
}

void
text_kill (Text killed, Direction d, Text t, Coord p, int length)
{
  assert (0 <= length);		/* TODO: eliminate? */
  Coord k = (forward == d ? text_end (killed) : 0);
  text_copy (killed, k, t, p, length);
  text_delete_span (t, p, length);
}


static Coord 
forward_clip (Text t, Coord p)
{
  return p == nowhere ? text_end (t) : p;
}

Coord
text_beginning_of_line (Text t, Coord p)
{
  /* This depends on nowhere == -1 */
  return 1 + text_find_char_set (t, p - 1, backward, newline);
}

Coord
text_end_of_line (Text t, Coord p)
{
  return forward_clip (t, text_find_char_set (t, p, forward, newline));
}


static Char
char_at (Text t, Coord p)
{
  Char c;
  text_get (t, p, 1, &c);
  return c;
}

static boolean
is_blank_line (Text t, Coord p)
{
  Coord q = text_find_char_set (t, p, forward, nonblank_chars);
  return nowhere == q || '\n' == char_at (t, q);
}

Coord 
text_beginning_of_paragraph (Text t, Coord p)
{
  Coord b = text_beginning_of_line (t, p);
  for (;;)
    {
      Coord back = text_beginning_of_line (t, b - 1);
      if (back == b || is_blank_line (t, back))
	return b;
      b = back;
    }
}


/* TODO: factor out common logic */
Coord
text_backward_word (Text t, Coord p)
{
  Coord i, j;

  i = p - 1;
  if (i == nowhere) return 0;
  i = text_find_char_set (t, i, backward, word_chars);
  if (i == nowhere) return 0;
  j = text_find_char_set (t, i, backward, nonword_chars);
  ++j;				/* depends on nowhere == -1 */

  return j;
}

Coord
text_forward_word (Text t, Coord p)
{
  Coord i = forward_clip (t, text_find_char_set (t, p, forward, word_chars));
  Coord j = forward_clip (t, text_find_char_set (t, i, forward, nonword_chars));
  return j;
}


void
text_insert_char (Text t, Coord p, Char c)
{
  text_replace (t, p, 0, &c, 1);
}


enum { chunk_size = 1024 };

void
text_insert_file (Text t, Coord p, const char *filename)
{
  FILE *f = fopen (filename, "r");
  if (f == NULL)
    stdlib_error (filename);
  else
    {
      Char chars [chunk_size];
      for (;;)
	{
	  size_t n = fread (chars, sizeof *chars, chunk_size, f);
	  text_replace (t, p, 0, chars, n);
	  p += n;
	  if (n < chunk_size) break;
	}
      if (ferror (f))
	stdlib_error (filename);
      fclose (f);
    }
}

void
text_write_file (Text t, Coord p, int length, 
		 const char *filename)
{
  FILE *f = fopen (filename, "w");
  if (f == NULL)
    stdlib_error (filename);
  else
    {
      while (0 < length)
	{
	  Char chars [chunk_size];
	  int count = min (length, chunk_size);
	  text_get (t, p, count, chars);
	  if (count != fwrite (chars, sizeof *chars, count, f))
	    stdlib_error (filename);
	  length -= count, p += count;
	}
      fclose (f);
    }
}


static boolean
matches (Text t1, Coord p1, Text t2, Coord p2, int length)
{
  int i;
  for (i = 0; i < length; ++i)
    {
      Char c1, c2;
      text_get (t1, p1+i, 1, &c1);
      text_get (t2, p2+i, 1, &c2);
      if (c1 != c2)
	return no;
    }
  return yes;
}

static Coord
text_search (Text t, Coord p, Text target, Coord length)
{
  Coord end = text_end (t);
  for (; p + length <= end; ++p)
    if (matches (target, 0, t, p, length))
      return p;
  return nowhere;
}


/* Starting at 'p' and extending to the end of the paragraph (a
   newline followed by a blank line, or end of text), greedily
   reformat so each line has length <= 'width', where possible. 
   'column' is the display-position of coordinate 'p', e.g. 
   column==0 means the left side, column==width means the right side. */
void
text_fill_paragraph (Text t, Coord p, int column, int width)
{
  for (;;)
    {
      { /* Delete any blanks at p. */
	int w = text_find_char_set (t, p, forward, nonblank_chars);
	text_delete_span (t, p, forward_clip (t, w) - p);
	if (nowhere == w)
	  break;
      }
      /* Now p is on a nonblank. */
      if ('\n' == char_at (t, p))
	{ /* Check for end of paragraph. */
	  int w2 = text_find_char_set (t, p + 1, forward, nonblank_chars);
	  if (nowhere == w2 || '\n' == char_at (t, w2))
	    break;
	  /* Delete blanks up to and including the newline. */
	  text_delete_span (t, p, w2 - p);
	  continue;
	}
      { /* Now p is on a nonblank, non-newline. */
	int w3 = text_find_char_set (t, p, forward, blank_or_newline_chars);
	int length = forward_clip (t, w3) - p;
	/* Insert the appropriate separator. */
	if (0 == column)
	  ;
	else if (column + 1 + length <= width)
	  text_replace (t, p++, 0, " ", 1), ++column;
	else
	  text_replace (t, p++, 0, "\n", 1), column = 0;
	/* Skip over the nonblank text. */
	p += length, column += length;
      }
    }
}


void
buffer_insert_char (Buffer b, Char c)
{
  text_insert_char (buffer_text (b), buffer_point (b), c);
}


void
buffer_delete_span (Buffer b, int count)
{
  text_delete_span (buffer_text (b), buffer_point (b), count);
}


/* Pre: start <= end */
/* TODO: we should probably drop the d argument by allowing end to 
   precede start */
void
buffer_kill (Buffer b, Direction d, Coord start, Coord end)
{
  Text killed = editor_killed (the_editor);
  
  if (0 == (attr_kill & buffer_previous_command (b)))
    text_delete_span (killed, 0, text_end (killed));

  text_kill (killed, d, buffer_text (b), start, end - start);
}

void
buffer_yank (Buffer b)
{
  Text killed = editor_killed (the_editor);
  text_copy (buffer_text (b), buffer_point (b), killed, 0, text_end (killed));
}


void
buffer_move (Buffer b, int offset)
{
  buffer_move_to (b, buffer_point (b) + offset);
}


void
buffer_backward_word (Buffer b)
{
  buffer_move_to (b, text_backward_word (buffer_text (b),
					 buffer_point (b)));
}

void
buffer_forward_word (Buffer b)
{
  buffer_move_to (b, text_forward_word (buffer_text (b),
					buffer_point (b)));
}


void
buffer_kill_line (Buffer b)
{
  Coord p = buffer_point (b);
  Coord eol = text_end_of_line (buffer_text (b), p);

  /* If at end of line already, kill newline, else kill to end: */
  buffer_kill (b, forward, p, eol == p ? eol + 1 : eol);
}


void
buffer_kill_region (Buffer b)
{
  Coord p = buffer_point (b), q = buffer_mark (b);
  if (q != nowhere)
    {
      if (p < q)
	buffer_kill (b, forward, p, q);
      else
	buffer_kill (b, backward, q, p);
    }
}

void
buffer_kill_ring_save (Buffer b)
{
  Text t = buffer_text (b), killed = editor_killed (the_editor);
  Coord p = buffer_point (b), q = buffer_mark (b);
  if (q != nowhere)		/* TODO factor out duplication */
    {
      if (p < q)
	text_copy (killed, text_end (killed), t, p, q - p);
      else
	text_copy (killed, 0, t, q, p - q);
    }
}


void
buffer_kill_word (Buffer b)
{
  Coord p = buffer_point (b);
  buffer_kill (b, forward, p, text_forward_word (buffer_text (b), p));
}

void
buffer_backward_kill_word (Buffer b)
{
  Coord p = buffer_point (b);
  buffer_kill (b, backward, text_backward_word (buffer_text (b), p), p);
}


void
buffer_beginning_of_line (Buffer b)
{
  buffer_move_to (b, text_beginning_of_line (buffer_text (b), 
					     buffer_point (b)));
}

void
buffer_end_of_line (Buffer b)
{
  buffer_move_to (b, text_end_of_line (buffer_text (b), 
				       buffer_point (b)));
}


void
buffer_next_line (Buffer b)
{
  Text t = buffer_text (b);
  Style s = buffer_style (b);
  Coord p = buffer_point (b);
  Coord eol = text_end_of_line (t, p);

  if (0 == (attr_goal_column & buffer_previous_command (b)))
    buffer_set_goal_column (b, style_column (s, t, p));

  buffer_move_to (b, style_forward_columns (s, t, eol + 1, 
					    buffer_goal_column (b)));
}

void
buffer_previous_line (Buffer b)
{
  Text t = buffer_text (b);
  Style s = buffer_style (b);
  Coord p = buffer_point (b);
  Coord bol = text_beginning_of_line (t, p);

  if (0 == (attr_goal_column & buffer_previous_command (b)))
    buffer_set_goal_column (b, style_column (s, t, p));

  if (bol == 0)
    buffer_move_to (b, 0);
  else
    {
      Coord bopl = text_beginning_of_line (t, bol - 1);
      buffer_move_to (b, style_forward_columns (s, t, bopl,
						buffer_goal_column (b)));
    }
}


void 
buffer_loudly_set_mark (Buffer b)
{
  buffer_set_mark (b, buffer_point (b));
  warn ("Mark set");
}

void
buffer_swap_point_and_mark (Buffer b)
{
  Coord m = buffer_mark (b);
  if (m == nowhere) return;
  buffer_set_mark (b, buffer_point (b));
  buffer_move_to (b, m);
}


void
buffer_save_file (Buffer b)
{
  Text t = buffer_text (b);
  const char *filename = buffer_filename (b);
  if (filename == NULL)
    warn ("No file to save to!");
  else
    {
      backup_file (filename);
      text_write_file (t, 0, text_end (t), filename);
      warn ("Saved.");
    }
}


void
buffer_transpose_chars (Buffer b)
{
  /* TODO: check whether we're doing the right thing to any marks set
     in the two-character range we mutate. */
  Coord p = buffer_point (b);
  Text t = buffer_text (b);
  Coord end = text_end (t);
  if (p == 0 || p == end || end < 2) /* not quite like emacs... */
    return;
  {
    Char pair [2];
    text_get (t, p - 1, 2, pair);
    {				/* TODO: handle end of line specially */
      Char c = pair [0];
      pair [0] = pair [1];
      pair [1] = c;
    }
    text_replace (t, p - 1, 2, pair, 2);
  }
}


void
buffer_downcase_word (Buffer b)
{
  Text t = buffer_text (b);
  Coord p = buffer_point (b);
  Coord q = text_forward_word (t, p);

  /* Convert one character at a time to leave any marks less disturbed */
  /* TODO: is that enough?  is there a better way? */
  for (; p < q; ++p)
    {
      Char c, d;
      text_get (t, p, 1, &c);
      d = tolower(c);
      text_replace (t, p, 1, &d, 1);
    }

  buffer_move_to (b, p);
}

void
buffer_upcase_word (Buffer b)
{
  Text t = buffer_text (b);
  Coord p = buffer_point (b);
  Coord q = text_forward_word (t, p);

  /* Convert one character at a time to leave any marks less disturbed */
  /* TODO: is that enough?  is there a better way? */
  for (; p < q; ++p)
    {
      Char c, d;
      text_get (t, p, 1, &c);
      d = toupper(c);
      text_replace (t, p, 1, &d, 1);
    }

  buffer_move_to (b, p);
}


boolean
buffer_search (Buffer b, Coord p, Text target, Coord length)
{
  Coord at = text_search (buffer_text (b), p, target, length);
  if (nowhere != at)
    {
      buffer_move_to (b, at);
      return yes;
    }
  return no;
}


void
buffer_fill_paragraph (Buffer b, int width)
{
  Text t = buffer_text (b);
  Coord p = text_beginning_of_paragraph (t, buffer_point (b));
  text_fill_paragraph (t, p, 0, width);
}


Coord
display_find_row (Display d, int row)
{
  Coord row_start = display_start (d);
  int r;
  for (r = 0; r < row; ++r)
    {
      Coord next_row_start = display_next_row (d, row_start);
      if (next_row_start == row_start)
	return nowhere;		/* Display ends before row is reached */
      row_start = next_row_start;
    }
  return row_start;
}


Coord
display_nth_previous_row (Display d, Coord start, int n)
{
  while (0 < n && 0 < start)
    --n, start = display_previous_row (d, start);
  return start;
}


void
display_scroll_up (Display d)
{
  /* FIXME: deal with height <= 2 */
  Coord penultimate = display_find_row (d, display_height (d) - 2);
  if (penultimate == nowhere)
    return;
  buffer_move_to (display_buffer (d), penultimate);
  display_scroll_to (d, penultimate);
}

void
display_scroll_down (Display d)
{
  Coord original_start = display_start (d);

  /* FIXME: deal with height <= 2 */
  Coord row = display_nth_previous_row (d, original_start, 
					display_height (d) - 2);
  display_scroll_to (d, row);
  /* FIXME: shouldn't move point if it'd be in the display already */
  buffer_move_to (display_buffer (d), original_start);
}


void
display_scroll_to_end (Display d)
{
  Buffer b = display_buffer (d);
  Coord end = text_end (buffer_text (b));
  Coord end_row = display_beginning_of_row (d, end);
  buffer_move_to (b, end);
  /* FIXME: don't scroll if point is still visible */
  /* FIXME: deal with height <= 2 */
  display_scroll_to (d, 
		     display_nth_previous_row (d, end_row,
					       display_height (d) - 2));
}
