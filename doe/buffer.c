/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"

struct Buffer {
  Text text;
  const char *filename;
  Mark point, mark, page;

  Command_attributes previous_command;

  int goal_column;
  Style style;
};


void
buffer_check (Buffer b)
{
  if (debug)
    {
      assert (b != NULL);
      text_check (b->text);
      mark_check (b->point, b->text);
      assert (mark_direction (b->point) == forward);
      if (b->mark)
	{
	  mark_check (b->mark, b->text);
	  assert (mark_direction (b->mark) == backward);
	}
      mark_check (b->page, b->text);
      assert (mark_direction (b->page) == backward);

      assert (0 <= b->goal_column);
      style_check (b->style);
    }
}


Buffer 
buffer_make (Text t, Style s, const char *filename)
{
  Buffer b = allot (sizeof *b);
  
  b->text = t;
  b->filename = filename;
  b->point = mark_make (t, 0, forward);
  b->mark = NULL;
  b->page = mark_make (t, 0, backward);

  b->previous_command = 0;

  b->goal_column = 0;
  b->style = s;

  buffer_check (b);
  return b;
}


void
buffer_unmake (Buffer b)
{
  buffer_check (b);
  mark_unmake (b->page);
  if (b->mark)
    mark_unmake (b->mark);
  mark_unmake (b->point);
  unallot (b);
}


Coord
buffer_point (Buffer b)
{
  buffer_check (b);
  return mark_coord (b->point);
}


void
buffer_move_to (Buffer b, Coord p)
{
  buffer_check (b);
  mark_unmake (b->point);
  b->point = mark_make (b->text, p, forward);
  buffer_check (b);
}


Coord
buffer_mark (Buffer b)
{
  Mark m = b->mark;
  buffer_check (b);
  return (m ? mark_coord (m) : nowhere);
}


void
buffer_set_mark (Buffer b, Coord p)
{
  buffer_check (b);
  if (b->mark) mark_unmake (b->mark);
  b->mark = (p == nowhere ? NULL : mark_make (b->text, p, backward));
  buffer_check (b);
}


Coord
buffer_page (Buffer b)
{
  buffer_check (b);
  return mark_coord (b->page);
}


void
buffer_set_page (Buffer b, Coord p)
{
  buffer_check (b);
  mark_unmake (b->page);
  b->page = mark_make (b->text, p, backward);
  buffer_check (b);
}


Text
buffer_text (Buffer b)
{
  buffer_check (b);
  return b->text;
}


const char *
buffer_filename (Buffer b)
{
  buffer_check (b);
  return b->filename;
}


Style
buffer_style (Buffer b)
{
  buffer_check (b);
  return b->style;
}


Command_attributes
buffer_previous_command (Buffer b)
{
  buffer_check (b);
  return b->previous_command;
}


void
buffer_set_previous_command (Buffer b, Command_attributes previous)
{
  buffer_check (b);
  b->previous_command = previous;
}


int
buffer_goal_column (Buffer b)
{
  buffer_check (b);
  return b->goal_column;
}


void
buffer_set_goal_column (Buffer b, int column)
{
  buffer_check (b);
  assert (0 <= column);
  b->goal_column = column;
  buffer_check (b);
}
