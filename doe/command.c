/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"

#include <curses.h>
#include <string.h>

void
cmd_keep_looking (int key, Frame f, Display d, Buffer b, Text t)
{
}

static Command
get_default_command (Binding *rib)
{
  int i = 0;
  for (;; ++i)
    {
      int entry = rib [i].key;
      if (entry == any_key)
	return rib [i].command;
    }
  unreachable;
}

static int
count_ribs (Key_map map)
{
  int i = 0;
  for (;; ++i)
    if (cmd_keep_looking != get_default_command (map [i]))
      return i + 1;
  unreachable;
}

Key_map
key_map_extend (Key_map map, Binding *bindings)
{
  int ribs = count_ribs (map);
  Key_map result = allot ((ribs + 1) * sizeof result[0]);
  result[0] = bindings;
  memcpy (&result[1], map, ribs * sizeof result[1]);
  return result;
}

static Binding *
keyrib_lookup (Binding *rib, int key)
{
  int i = 0;
  for (;; ++i)
    {
      int entry = rib [i].key;
      if (entry == any_key || entry == key)
	return &rib [i];
    }
  unreachable;
}

static Binding *
keymap_lookup (Key_map map, int key)
{
  int i = 0;
  for (;; ++i)
    {
      Binding *binding = keyrib_lookup (map [i], key);
      if (binding->command != cmd_keep_looking)
	return binding;
    }
  unreachable;
}


/* Global state variables available to commands: */
static Key_map current_keymap;
static boolean quitting;

static Key_map C_q_keymap_ptr;  /* ugh */
static Key_map C_s_keymap_ptr;  /* ugh */
static Key_map C_x_keymap_ptr;  /* ugh */
static Key_map minibuffer_keymap_ptr;  /* ugh */


static void
erase_message (Frame f)
{
  frame_set_message (f, "");
}


void
warn (const char *message)
{
  Frame f = editor_frame (the_editor);
  assert (f != NULL);
  frame_set_message (f, message);
}


static void
prefix (Frame f, const char *message, Key_map keymap)
{
  current_keymap = keymap;
  frame_set_message (f, message);
}


static void
unprefix (Frame f)
{
  /* FIXME: might need to go to minibuffer_keymap.  oops. */
  current_keymap = editor_fundamental_keymap (the_editor);
  frame_set_active_display (f, frame_nth_display (f, 0));
}


static void
flush_minibuffer (Frame f, const char *message)
{
  Text t = buffer_text (display_buffer (frame_minibuffer (f)));
  text_replace (t, 0, text_end (t), "", 0);
  unprefix (f);
  frame_set_message (f, message);
}


static void
search_from_minibuffer (Frame f, int starting_offset)
{
  Text target = buffer_text (display_buffer (frame_minibuffer (f)));
  Buffer b = display_buffer (frame_nth_display (f, 0));
  Coord p = buffer_point (b);
  buffer_search (b, p + starting_offset, target, text_end (target));
}


static Buffer
find_next_buffer (Buffer b)
{
  Name_table nt = editor_name_table (the_editor);
  int i, count = name_table_count (nt);
  for (i = 0; i < count; ++i)
    {
      Buffer bi = name_table_ref (nt, name_table_nth_key (nt, i));
      if (bi == b)
	return name_table_ref (nt, name_table_nth_key (nt, (i + 1) % count));
    }
  unreachable;
  return b;
}

static void
switch_current_buffer (Frame f, Buffer b)
{
  Buffer nb = find_next_buffer (b);
  if (nb == b)
    warn ("No other buffer");
  frame_replace_active_display (f, nb);
}


/* Random hack -- remapping '[]' keys to '()' for Lisp. */
static int
tweak (int key)
{
  switch (key)
    {
    case '[': return '(';
    case ']': return ')';
    case '(': return '[';
    case ')': return ']';
    default: return key;
    }
}


DEFCMD(C_q)                  { prefix (f, "C-q", C_q_keymap_ptr); }
DEFCMD(C_x)                  { prefix (f, "C-x", C_x_keymap_ptr); }
DEFCMD(abort)                { flush_minibuffer (f, "Quit"); }
DEFCMD(backward_char)        { buffer_move (b, -1); }
DEFCMD(backward_word)        { buffer_backward_word (b); }
DEFCMD(backward_kill_word)   { buffer_backward_kill_word (b); }
DEFCMD(beginning_of_buffer)  { buffer_loudly_set_mark (b);
                               buffer_move_to (b, 0); }
DEFCMD(beginning_of_line)    { buffer_beginning_of_line (b); }
DEFCMD(delete_char)          { buffer_delete_span (b, 1); }
DEFCMD(delete_backward_char) { buffer_delete_span (b, -1); }
DEFCMD(downcase_word)        { buffer_downcase_word (b); }
DEFCMD(end_of_buffer)        { buffer_loudly_set_mark (b);
                               display_scroll_to_end (d); }
DEFCMD(end_of_line)          { buffer_end_of_line (b); }
DEFCMD(fill_paragraph)       { buffer_fill_paragraph (b, 72); }
DEFCMD(forward_char)         { buffer_move (b, 1); }
DEFCMD(forward_word)         { buffer_forward_word (b); }
DEFCMD(isearch_forward)      { prefix (f, "C-s", C_s_keymap_ptr); 
                               buffer_set_mark (b, buffer_point (b)); }
DEFCMD(isearch_insert)       { Buffer mb =
				 display_buffer (frame_minibuffer (f));
                               buffer_insert_char (mb, key); 
                               search_from_minibuffer (f, 0); }
DEFCMD(isearch_backspace)    { Buffer mb =
				 display_buffer (frame_minibuffer (f));
                               buffer_delete_span (mb, -1);
                               buffer_move_to (b, buffer_mark (b));
                               search_from_minibuffer (f, 0); }
DEFCMD(isearch_again)        { Buffer mb = 
				 display_buffer (frame_minibuffer (f));
                               int offset = text_end (buffer_text (mb));
                               search_from_minibuffer (f, offset); } 
DEFCMD(isearch_done)         { flush_minibuffer (f, 
				 "Mark saved where search started"); }
DEFCMD(kill_line)            { buffer_kill_line (b); }
DEFCMD(kill_region)          { buffer_kill_region (b); }
DEFCMD(kill_ring_save)       { buffer_kill_ring_save (b); }
DEFCMD(kill_word)            { buffer_kill_word (b); }
DEFCMD(long_command)         { prefix (f, "M-x", minibuffer_keymap_ptr); 
                               frame_set_active_display (
				 f, frame_minibuffer (f)); }
DEFCMD(minibuffer_enter)     { /* TODO: fill in with commands */
                               flush_minibuffer (f, "Unimplemented"); }
DEFCMD(newline)              { buffer_insert_char (b, '\n'); }
DEFCMD(next_line)            { buffer_next_line (b); }
#if 0
DEFCMD(open_line)            { buffer_insert_char (b, '\n'); 
                               buffer_move (b, -1); }
#endif
DEFCMD(previous_line)        { buffer_previous_line (b); }
DEFCMD(quit)                 { quitting = yes; }
DEFCMD(recenter)             { display_center (d); }
DEFCMD(save_and_switch_buffer) { buffer_save_file (b);
                               switch_current_buffer (f, b); }
DEFCMD(save_buffer)          { buffer_save_file (b);
                               unprefix (f); }
DEFCMD(scroll_down)          { display_scroll_down (d); }
DEFCMD(scroll_up)            { display_scroll_up (d); }
DEFCMD(self_insert)          { buffer_insert_char (b, key); }
DEFCMD(self_insert_C_q)      { buffer_insert_char (b, key); 
                               unprefix (f); }
DEFCMD(set_mark)             { buffer_loudly_set_mark (b); }
DEFCMD(swap_point_and_mark)  { buffer_swap_point_and_mark (b); 
                               unprefix (f); }
DEFCMD(switch_to_buffer)     { /* Not like the Emacs command currently */
                               switch_current_buffer (f, b); }
DEFCMD(tweaked_insert)       { buffer_insert_char (b, tweak (key)); }
DEFCMD(transpose_chars)      { buffer_transpose_chars (b); }
DEFCMD(unbound_key)          { unprefix (f); }
DEFCMD(upcase_word)          { buffer_upcase_word (b); }
DEFCMD(yank)                 { buffer_yank (b); }


/* the C-spacebar key: */
enum { C_SPACE = 0 };

static Binding C_q_keyrib[] = {
  /* The last entry of a keymap must always be for any_key: */
  { any_key,              0,         cmd_self_insert_C_q } 
};

static Binding *C_q_keymap[] = { C_q_keyrib };

static Binding C_s_keyrib[] = {
  { '\r',                 0,         cmd_isearch_done },
  /* TODO isearch_done also on other special keys */
  { KEY_BACKSPACE,        0,         cmd_isearch_backspace },
  { CTRL('G'),            0,         cmd_abort }, /* FIXME also reset pos */
  { CTRL('S'),            0,         cmd_isearch_again },
  { any_key,              0,         cmd_isearch_insert } 
};

static Binding *C_s_keymap[] = { C_s_keyrib };

static Binding common_keyrib[] = {
  { CTRL('G'),            0,         cmd_abort },
  { any_key,              0,         cmd_unbound_key }
};

static Binding C_x_keyrib[] = {
/*{ '0',                  0,         cmd_delete_window },*/
/*{ '1',                  0,         cmd_delete_other_window },*/
/*{ '2',                  0,         cmd_split_window_vertically },*/
/*{ '3',                  0,         cmd_split_window_horizontally },*/
/*{ 'b',                  0,         cmd_switch_to_buffer },*/
/*{ 'i',                  0,         cmd_insert_file },*/
/*{ 'k',                  0,         cmd_kill_buffer },*/
/*{ 'o',                  0,         cmd_other_window },*/
/*{ 'u',                  0,         cmd_undo }, */
/*{ '\t'                  0,         cmd_indent_rigidly }, */
/*{ CTRL('B'),            0,         cmd_list_buffers },*/
  { CTRL('C'),            0,         cmd_quit }, 
/*{ CTRL('F'),            0,         cmd_find_file },*/
  { CTRL('S'),            0,         cmd_save_buffer },
/*{ CTRL('V'),            0,         cmd_find_alternate_file },*/
/*{ CTRL('W'),            0,         cmd_write_file },*/
  { CTRL('X'),            0,         cmd_swap_point_and_mark },
  { any_key,              0,         cmd_keep_looking }
};

static Binding *C_x_keymap[] = { C_x_keyrib, common_keyrib };

static Binding root_keyrib[] = {
  { '\r',                 0,         cmd_newline },
  { KEY_BACKSPACE,        0,         cmd_delete_backward_char },
  { KEY_LEFT,             0,         cmd_backward_char },
  { KEY_RIGHT,            0,         cmd_forward_char },
  { KEY_DOWN,     attr_goal_column, cmd_next_line },
  { KEY_UP,       attr_goal_column, cmd_previous_line },
  { KEY_NPAGE,            0,         cmd_scroll_up },
  { KEY_PPAGE,            0,         cmd_scroll_down },
  { C_SPACE,              0,         cmd_set_mark },
/*{ CTRL('/'),            0,         cmd_undo }, */
  { CTRL('A'),            0,         cmd_beginning_of_line },
  { CTRL('B'),            0,         cmd_backward_char },
  { CTRL('D'),            0,         cmd_delete_char },
  { CTRL('E'),            0,         cmd_end_of_line },
  { CTRL('F'),            0,         cmd_forward_char },
  { CTRL('G'),            0,         cmd_abort },
  { CTRL('H'),            0,         cmd_delete_backward_char },
  { CTRL('K'),            attr_kill, cmd_kill_line },
  { CTRL('L'),            0,         cmd_recenter },
  { CTRL('N'),     attr_goal_column, cmd_next_line },
  { CTRL('O'),            0,         /* In Emacs this is cmd_open_line */
                                     cmd_save_and_switch_buffer }, 
  { CTRL('P'),     attr_goal_column, cmd_previous_line },
  { CTRL('Q'),            0,         cmd_C_q },
/*{ CTRL('R'),            0,         cmd_isearch_backward }, */
  { CTRL('S'),            0,         cmd_isearch_forward },
  { CTRL('T'),            0,         cmd_transpose_chars },
/*{ CTRL('U'),            0,         cmd_universal_argument }, */
  { CTRL('V'),            0,         cmd_scroll_up },
  { CTRL('W'),            attr_kill, cmd_kill_region },
  { CTRL('X'),            0,         cmd_C_x },
  { CTRL('Y'),            0,         cmd_yank },
  { META('b'),            0,         cmd_backward_word },
/*{ META('c'),            0,         cmd_capitalize_word }, */
  { META('d'),            attr_kill, cmd_kill_word },
  { META('f'),            0,         cmd_forward_word },
  { META('l'),            0,         cmd_downcase_word },
  { META('q'),            0,         cmd_fill_paragraph },
/*{ META('t'),            0,         cmd_transpose_words }, */
  { META('u'),            0,         cmd_upcase_word },
  { META('v'),            0,         cmd_scroll_down },
  { META('w'),            0,         cmd_kill_ring_save },
  { META('x'),            0,         cmd_long_command },
/*{ META('y'),            0,         cmd_yank_pop }, */
  { META(KEY_BACKSPACE), attr_kill, cmd_backward_kill_word },
  { META(KEY_LEFT),       0,         cmd_backward_word },
  { META(KEY_RIGHT),      0,         cmd_forward_word },
  { META(CTRL('<')),      0,         cmd_beginning_of_buffer },
  { META(CTRL('>')),      0,         cmd_end_of_buffer },
  { META(CTRL('H')),      attr_kill, cmd_backward_kill_word },
#if 0
  { '[',                  0,         cmd_tweaked_insert },
  { ']',                  0,         cmd_tweaked_insert },
  { '(',                  0,         cmd_tweaked_insert },
  { ')',                  0,         cmd_tweaked_insert },
#endif
  { any_key,              0,         cmd_self_insert }
};

static Binding nonemacs_keyrib[] = {
  { META('o'),            0,         cmd_switch_to_buffer },
  { any_key,              0,         cmd_keep_looking }
};

Binding *doe_default_root_keymap[] = { nonemacs_keyrib, root_keyrib };

static Binding minibuffer_keyrib[] = {
  { '\r',                 0,         cmd_minibuffer_enter },
  { CTRL('L'),            0,         cmd_unbound_key },
  { CTRL('N'),            0,         cmd_unbound_key },
  { CTRL('O'),            0,         cmd_unbound_key },
  { CTRL('P'),            0,         cmd_unbound_key },
  { CTRL('V'),            0,         cmd_unbound_key },
  { any_key,              0,         cmd_keep_looking }
};

static Binding *minibuffer_keymap[] = { 
  minibuffer_keyrib, root_keyrib 
};


void
interactor_loop (Frame f)
{
  quitting = no;
  C_q_keymap_ptr = C_q_keymap;
  C_s_keymap_ptr = C_s_keymap;
  C_x_keymap_ptr = C_x_keymap;
  minibuffer_keymap_ptr = minibuffer_keymap;
  current_keymap = editor_fundamental_keymap (the_editor);

  for (;;)
    {
      frame_update (f);
      {
	int key = terminal_get_key (frame_terminal (f));
	Binding *binding = keymap_lookup (current_keymap, key);

	Display d = frame_active_display (f);
	Buffer b = display_buffer (d);
	if (current_keymap != minibuffer_keymap_ptr && /* ugh */
	    current_keymap != C_s_keymap_ptr)
	  erase_message (f);
	binding->command (key, f, d, b, buffer_text (b));

	buffer_set_previous_command (b, binding->attributes);
	if (quitting) break;
      }
    }
}
