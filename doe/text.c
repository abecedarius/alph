/* Copyright 2000 by Darius Bacon; see the file COPYING. */

#include "doe.h"
#include <string.h>

typedef struct Mark_list *Mark_list;

struct Mark {
  struct Text *text;
  Coord coord;
  Direction direction;
};

struct Mark_list {
  int count, limit;
  Mark *array;
};

struct Text {
  Char *chars;
  int gap, after, size;
  struct Mark_list marks;
};


void
direction_check (Direction d)
{
  assert (d == backward || d == forward);
}


void
mark_check (Mark m, Text t)
{
  if (debug)
    {
      assert (m != NULL);
      direction_check (m->direction);
      assert (0 <= m->coord && m->coord <= text_end (t));
      assert (m->text == t);
    }
}


static void 
mark_list_check (Mark_list ms, Text t)
{
  assert (ms != NULL);
  assert (0 <= ms->count && ms->count <= ms->limit);
  assert (ms->limit == 0 || ms->array != NULL);
  {
    int i;
    for (i = ms->count - 1; 0 <= i; --i)
      mark_check (ms->array [i], t);
  }
}


void
text_check (Text t)
{
  if (debug)
    {
      assert (t != NULL);
      assert (t->chars != NULL);
      assert (0 <= t->gap && t->gap <= t->after && t->after <= t->size);
      mark_list_check (&t->marks, t);
    }
}


Text
text_make (int size)
{
  Text t = allot (sizeof *t);

  assert (0 <= size);
  t->chars = allot (size);
  t->gap = 0;
  t->after = size;
  t->size = size;
  t->marks.count = 0;
  t->marks.limit = 0;
  t->marks.array = NULL;

  text_check (t);
  return t;
}


void
text_unmake (Text t)
{
  Mark_list ms = &t->marks;
  text_check (t);
  assert (ms->count == 0);
  unallot (ms->array);
  unallot (t->chars);
  unallot (t);
}


Coord
text_end (Text t)
{
  /*  text_check (t);  recursion */
  return t->gap + (t->size - t->after);
}


Text
mark_text (Mark m)
{
  mark_check (m, m->text);
  return m->text;
}


Coord
mark_coord (Mark m)
{
  mark_check (m, m->text);
  return m->coord;
}


Direction
mark_direction (Mark m)
{
  mark_check (m, m->text);
  return m->direction;
}


static Coord
clip (Text t, Coord p)
{
  return p < 0 ? 0 : min (p, text_end (t));
}


static Mark
new_mark (Text t, Coord p, Direction d)
{
  Mark m = allot (sizeof *m);
  m->text = t;
  m->coord = clip (t, p);
  m->direction = d;
  mark_check (m, t);
  return m;
}


/* Return a new mark inside `t' at position `p'.  When there's an
   insertion at the mark's position, the mark will get pushed in
   direction `d'.  (`p' gets clipped to t's range.) */
Mark
mark_make (Text t, Coord p, Direction d)
{
  int limit = t->marks.limit;
  text_check (t);
  
  /* Expand the array if it's too small: */
  if (t->marks.count == limit)
    {
      limit += limit/2 + 4;	/* FIXME: handle overflow */
      t->marks.array = reallot (t->marks.array, 
				limit * sizeof t->marks.array[0]);
      t->marks.limit = limit;
    }

  {
    Mark m = new_mark (t, p, d);
    t->marks.array [t->marks.count++] = m;
    text_check (t);
    return m;
  }
}


void
mark_unmake (Mark m)
{
  Text t = m->text;
  Mark_list ms = &t->marks;
  int i;
  mark_check (m, t);

  for (i = ms->count - 1; 0 <= i; --i)
    if (ms->array [i] == m)
      {
	memmove (ms->array + i,
		 ms->array + i + 1,
		 sizeof (ms->array [0]) * (ms->count - i - 1));
	--ms->count;
	unallot (m);
	return;
      }

  unreachable;
}


/* Return the position in `t' of the instance of `cs' closest to `p' in
  direction `d'.  (If there's none, return `nowhere'.) */
Coord
text_find_char_set (Text t, Coord p, Direction d, Char_set cs)
{
  int gap = t->gap, after = t->after, size = t->size;
  const Char *chars = t->chars;
  int s = p;
  text_check (t);

  if (d == forward)
    {
      for (; s < gap; ++s)
	if (char_set_has (cs, chars [s]))
	  return s;
      for (s += after - gap; s < size; ++s)
	if (char_set_has (cs, chars [s]))
	  return s - (after - gap);
    }
  else
    {				/* d == backward */
      if (gap <= s)
	{
	  for (s += after - gap; after <= s; --s)
	    if (char_set_has (cs, chars [s]))
	      return s - (after - gap);
	  s = gap - 1;
	}
      for (; 0 <= s; --s)
	if (char_set_has (cs, chars [s]))
	  return s;
    }

  return nowhere;
}

/* Copy the text in [start..start+length) into dest. */
void
text_get (Text t, Coord start, int length, Char *dest)
{
  text_check (t);
  assert (0 <= length);
  assert (dest != NULL);
  {
    int s = max (0, start);
    int d = 0, gap = t->gap, size = t->size;
    const Char *src = t->chars;
    
    /* TODO: these loops could be faster */
    for (; s < gap && d < length; ++s, ++d)
      dest [d] = src [s];
    /* FIXME: check for int overflow in the += */
    for (s += t->after - gap; s < size && d < length; ++s, ++d)
      dest [d] = src [s];
  }
  text_check (t);
}


/* Replace the `length' characters after `start' by src[0..src_size). */
void
text_replace (Text t, Coord start, int length,
	      const Char *src, int src_size)
{
  text_check (t);
  if (start < 0) start = 0;
  assert (0 <= length);
  length = clip (t, start + length) - start;
  assert (src != NULL);
  assert (0 <= src_size);
  {
    int gap = t->gap, after = t->after, size = t->size;
    Char *chars = t->chars;
    int delta = gap - start;

    /* Move the gap to coincide with `start': */
    if (0 < delta)
      memmove (chars + (after - delta), chars + start, delta);
    else
      memmove (chars + gap, chars + after, -delta);
    after -= delta;
    gap -= delta;

    /* Delete the next `length' characters: */
    if (size - after < length)
      length = size - after;
    after += length;

    /* Expand the memory space if it's too small: */
    if (after - gap < src_size)
      {
	int new_size = size + size/2 + src_size; /* FIXME: handle overflow */
	int new_after = new_size - (size - after);
	chars = t->chars = reallot (chars, new_size);
	memmove (chars + new_after, chars + after, size - after);
	size = t->size = new_size;
	after = new_after;
      }

    /* Copy in the new insertion: */
    memcpy (chars + gap, src, src_size);
    gap += src_size;

    /* Update the state variables: */
    t->gap = gap;
    t->after = after;

    /* Update the marks: */
    {
      Mark_list ms = &t->marks;
      int i;
      for (i = ms->count - 1; 0 <= i; --i)
	{
	  Mark m = ms->array [i];
	  Coord p = m->coord;
	  /* TODO: reorder tests for efficiency */
	  if (start + length < p)
	    m->coord -= length - src_size;
	  else if (start <= p)
	    m->coord = (m->direction == backward ? start : start + src_size);
	  else
	    ;
	}
    }
  }
  text_check (t);
}
