OFILES += doe/buffer.o doe/charset.o doe/command.o \
          doe/curses-terminal.o doe/derived.o doe/display.o doe/doe.o \
	  doe/editor.o doe/frame.o doe/helpers.o \
          doe/name_table.o doe/style.o doe/text.o

DOE_H := doe/doe.h

doe/doe.o:		doe/doe.h doe/doe.c
doe/buffer.o: 		doe/doe.h doe/buffer.c
doe/name_table.o:	doe/doe.h doe/name_table.c
doe/charset.o:		doe/doe.h doe/charset.c
doe/command.o:		doe/doe.h doe/command.c
doe/curses-terminal.o:	doe/doe.h doe/curses-terminal.c
doe/derived.o: 		doe/doe.h doe/derived.c
doe/display.o: 		doe/doe.h doe/display.c
doe/editor.o:		doe/doe.h doe/editor.c
doe/frame.o:		doe/doe.h doe/frame.c
doe/helpers.o:		doe/doe.h doe/helpers.c
doe/style.o:		doe/doe.h doe/style.c
doe/text.o: 		doe/doe.h doe/text.c
