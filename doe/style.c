/* Copyright 2000 by Darius Bacon; see the file COPYING. */

#include <ctype.h>

#include "doe.h"

struct Style {
  int tab_width;
}; 


void
style_check (Style s)
{
  if (debug)
    {
      assert (s != NULL);
      assert (0 < s->tab_width);
    }
}


Style
style_make (int tab_width)
{
  Style s = allot (sizeof *s);
  s->tab_width = tab_width;
  style_check (s);
  return s;
}


void
style_unmake (Style s)
{
  style_check (s);
  unallot (s);
}


/* Add the glyphs that make up c, when it appears at x-coordinate
   `column' within a display, to `array', and return their count.
   The array must have capacity for a count up to max(4, s->tab_width).
   The return count is at least 1. */
int
style_format (Style s, Char c, int column, Glyph *array)
{
  style_check (s);

  if (isprint (c))
    {
      array[0] = c;
      return 1;
    }
  else if (c == '\t')
    {
      int spaces = s->tab_width - (column % s->tab_width);
      int s = spaces;
      do { *array++ = ' '; } while (0 < --s);
      return spaces;
    }
  else if ((unsigned)c < ' ')	/* control character */
    {
      array[0] = '^';
      array[1] = '@' + c;
      return 2;
    }
  else				/* misc. unprintable, use octal */
    {
      array[0] = '\\';
      array[1] = '0' + ((c >> 6) & 3);
      array[2] = '0' + ((c >> 3) & 7);
      array[3] = '0' + ((c >> 0) & 7);
      return 4;
    }
}


Glyph style_dev_null[4];


/* Return the column number that p of t is at, when viewed in style s. */
int
style_column (Style s, Text t, Coord p)
{
  Coord q = text_beginning_of_line (t, p);
  int col = 0;
  style_check (s);
  while (q < p)
    {
      Char c;
      text_get (t, q++, 1, &c);
      col += style_format (s, c, col, style_dev_null);
    }
  return col;
}


/* Pre: `p' is the start of a line in `t', and 0 <= columns.
   Return the offset of the `columns'th column of that line (or
   the nearest offset to it), when viewed in style `s'. */
Coord
style_forward_columns (Style s, Text t, Coord p, int columns)
{
  int col = 0;
  int end = text_end (t);
  style_check (s);
  for (; col < columns && p < end; ++p)
    {
      Char c;
      text_get (t, p, 1, &c);
      if (c == '\n') break;
      col += style_format (s, c, col, style_dev_null);
    }
  return p;
}
