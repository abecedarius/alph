/* Copyright 2000 by Darius Bacon; see the file COPYING. */

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "doe.h"


void
error (const char *format, ...)
{
  va_list args;
  va_start (args, format);
  vfprintf (stderr, format, args);
  va_end (args);
  fprintf (stderr, "\n");
  exit (1);
}


void
stdlib_error (const char *context)
{
  error ("%s: %s", context, strerror (errno));
}


enum { show_malloc = 0 };

void *
allot_fn (const char *file, int line, size_t size)
{
  void *result = malloc (size);
  if (show_malloc)
    fprintf (stderr, "%s:%d allot %p %zu\n", file, line, result, size);
  if (result == NULL && size != 0)
    stdlib_error ("Out of memory");
  return result;
}


void *
reallot_fn (const char *file, int line, void *ptr, size_t new_size)
{
  void *result = realloc (ptr, new_size);
  if (result == NULL && new_size != 0)
    stdlib_error ("Out of memory");
  if (show_malloc)
    fprintf (stderr, "%s:%d reallot %p %zu %p\n", 
	     file, line, result, new_size, ptr);
  return result;
}


void
unallot_fn (const char *file, int line, void *ptr)
{
  if (show_malloc)
    fprintf (stderr, "%s:%d free %p\n", file, line, ptr);
  free (ptr);
}


int
max (int x, int y)
{
  return x < y ? y : x;
}


int
min (int x, int y)
{
  return x < y ? x : y;
}


void
boolean_check (boolean b)
{
  assert (b == no || b == yes);
}


static FILE *
open_file (const char *filename, const char *mode)
{
  FILE *f = fopen (filename, mode);
  if (NULL == f)
    stdlib_error (filename);
  return f;
}


enum { chunk_size = 1024 };

void
copy_file (const char *dest_filename, FILE *dest, 
	   const char *src_filename, FILE *src)
{
  Char chars[chunk_size];
  for (;;)
    {
      size_t n = fread (chars, sizeof *chars, chunk_size, src);
      if (n != fwrite (chars, sizeof *chars, n, dest))
	stdlib_error (dest_filename);
      if (n < chunk_size) break;
    }
  if (ferror (src))
    stdlib_error (src_filename);
}


void
backup_file (const char *filename)
{
  char *backup_name = allot (strlen (filename) + 2);
  sprintf (backup_name, "%s~", filename);
  {
    FILE *src = fopen (filename, "rb");
    /* TODO: if !src, make sure it's a missing file and not some other
       error */
    if (src) 
      {
	FILE *dest = open_file (backup_name, "wb");
	copy_file (backup_name, dest, filename, src);
	fclose (dest);
	fclose (src);
      }
  }
  unallot (backup_name);
}
