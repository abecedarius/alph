/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "doe.h"

struct Frame {
  Terminal term;
  int height, width;

  Display d;
  int dx, dy;

  Display minibuffer;
  int mbx, mby;

  Display message;
  int mx, my;

  Display active;
}; 


void
frame_check (Frame f)
{
  if (debug)
    {
      assert (f != NULL);
      assert (2 < f->height && 0 < f->width);

      terminal_check (f->term);

      /* TODO: check dx, dy, mbx, mby */

      display_check (f->d, display_buffer (f->d));
      assert (display_height (f->d) <= f->height);
      assert (display_width (f->d) <= f->width);

      display_check (f->message, display_buffer (f->message));
      display_check (f->minibuffer, display_buffer (f->minibuffer));
      assert (display_height (f->message) == 1);
      assert (display_height (f->minibuffer) == 1);
      assert (display_width (f->message) + display_width (f->minibuffer) 
	       <= f->width);

      assert (f->active == f->d || f->active == f->minibuffer);
    }
}


static Display
frame_display_make (Frame f, Buffer b)
{
  /* y - 2 leaves space for the mode line and minibuffer: */
  f->d = display_make (b, f->width, f->height - 2, yes);
  f->dx = 0;
  f->dy = 0;

  f->active = f->d;

  return f->d;
}


Frame
frame_make (Terminal term, Name_table nt)
{
  int x, y;
  terminal_get_size (&x, &y, term);
  assert (2 < y);
  assert (8 <= x);

  {
    Frame f = allot (sizeof *f);

    f->term = term;
    f->height = y;
    f->width = x;

    {
      Buffer b = buffer_make (text_make (0), style_make (8), NULL);
      name_table_add (nt, "=scratch=", b);
      frame_display_make (f, b);
    }

    {
      Buffer mb = buffer_make (text_make (0), style_make (8), NULL);
      f->minibuffer = display_make (mb, x - 4, 1, no);
      f->mbx = 4;
      f->mby = y - 1;
    }

    {
      Buffer m = buffer_make (text_make (0), style_make (8), NULL);
      f->message = display_make (m, 4, 1, no);
      f->mx = 0;
      f->my = y - 1;
    }

    frame_check (f);
    return f;
  }
}


static void
private_display_unmake (Display d)
{
  Buffer b = display_buffer (d);
  Style s = buffer_style (b);
  Text t = buffer_text (b);
  display_unmake (d);
  buffer_unmake (b);
  style_unmake (s);
  text_unmake (t);
}

void
frame_unmake (Frame f)
{
  frame_check (f);
  private_display_unmake (f->message);
  private_display_unmake (f->minibuffer);
  display_unmake (f->d);
  unallot (f);
}


Terminal
frame_terminal (Frame f)
{
  frame_check (f);
  return f->term;
}


Display
frame_minibuffer (Frame f)
{
  frame_check (f);
  return f->minibuffer;
}


void
frame_set_message (Frame f, const char *message)
{
  frame_check (f);
  {
    int length = strlen (message);
    Text t = buffer_text (display_buffer (f->message));
    text_replace (t, 0, text_end (t), message, length);
    
    /* This assumes 8 <= f->width. */
    /* (The +1 makes a space between message and minibuffer.) */
    length = max (4, min (length + 1, f->width - 4));
    display_set_width (f->message, length);
    display_set_width (f->minibuffer, f->width - length);
    f->mbx = length;
  }
  frame_check (f);
}


Display
frame_split (Frame f, Buffer b)
{
  unreachable;
  return NULL;
}


Display
frame_active_display (Frame f)
{
  frame_check (f);
  return f->active;
}


/* Pre: d is one of f's displays. */
void
frame_set_active_display (Frame f, Display d)
{
  frame_check (f);
  assert (f->d == d || f->minibuffer == d);
  f->active = d;
}


void
frame_replace_active_display (Frame f, Buffer b)
{
  display_unmake (frame_active_display (f));
  frame_display_make (f, b);
  frame_check (f);
}


int
frame_width (Frame f)
{
  frame_check (f);
  return f->width;
}


int
frame_height (Frame f)
{
  frame_check (f);
  return f->height;
}


static void
maybe_update (boolean active, Frame f, Display d, int x, int y)
{
  frame_check (f);
  if (active == (frame_active_display (f) == d))
    display_update (d, f->term, x, y);
}


void
frame_update (Frame f)
{
  frame_check (f);

  /* First update all the inactive displays */
  maybe_update (no, f, f->message, f->mx, f->my);
  maybe_update (no, f, f->minibuffer, f->mbx, f->mby);
  maybe_update (no, f, f->d, f->dx, f->dy);

  /* Then the active one.  It's last so the cursor ends up on it. */
  maybe_update (yes, f, f->message, f->mx, f->my);
  maybe_update (yes, f, f->minibuffer, f->mbx, f->mby);
  maybe_update (yes, f, f->d, f->dx, f->dy);

  terminal_refresh (f->term);

  frame_check (f);
}


int
frame_display_count (Frame f)
{
  frame_check (f);
  return 2;
}


Display
frame_nth_display (Frame f, int n)
{
  frame_check (f);
  switch (n)
    {
    case 0: return f->d;
    case 1: return f->minibuffer;
    default: unreachable; return NULL;
    }
}
