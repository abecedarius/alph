/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"

#include <curses.h>


void
terminal_check (Terminal term)
{
  if (debug)
    {
      assert (term != NULL);
      assert (0 <= term->x);
      assert (0 <= term->y);
    }
}

#include <stdio.h>

Terminal
terminal_make (void)
{
  Terminal term = allot (sizeof *term);
  term->x = term->y = 0;

  initscr ();
  raw ();
  noecho ();
  nonl ();
  intrflush (stdscr, FALSE);
  keypad (stdscr, TRUE);

  terminal_check (term);
  return term;
}


void
terminal_unmake (Terminal term)
{
  terminal_check (term);

  endwin ();
  unallot (term);
}


void
terminal_get_size (int *x, int *y, Terminal term)
{
  terminal_check (term);

  *x = COLS, *y = LINES;
}


void
terminal_refresh (Terminal term)
{
  terminal_check (term);

  refresh ();
}


void
terminal_move_to (Terminal term, int x, int y)
{
  terminal_check (term);

  move (y, x);
  term->x = x;
  term->y = y;

  terminal_check (term);
}


void
terminal_put_glyph (Terminal term, Glyph c)
{
  terminal_check (term);

  addch (c);
  term->x++;
}


void 
terminal_newline (Terminal term, int right, int left)
{
  int x = term->x;
  terminal_check (term);
  for (; x < right; ++x)	/* clear to end of line */
    addch (' ');
  term->x = left;
  term->y++;
  terminal_check (term);
}


void
terminal_normal_attribute (Terminal term)
{
  terminal_check (term);

  /*  attron (A_NORMAL); doesn't work, for some reason */
  attroff (A_REVERSE);
}


void
terminal_reverse_attribute (Terminal term)
{
  terminal_check (term);

  attron (A_REVERSE);
}


/* FIXME: don't need this anymore, probably */
static int
normalize (int key)
{
#if 0
  if (key == 0407)		/* another backspace variant. sheesh. */
    return KEY_BACKSPACE;
#endif
  return key;
}

int
terminal_get_key (Terminal term)
{
  int key = getch ();
  terminal_check (term);
  if (0) printf ("get_key -> %d\n", key);
  if (key == 27)		/* escape */
    return META (normalize (getch ()));
  return normalize (key);
}
