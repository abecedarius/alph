/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include <string.h>
#include <ctype.h>

#include "doe.h"

struct Display {
  Buffer buffer;
  int width, height;		/* not counting space for mode line */
  boolean has_mode_line;
}; 


void
display_check (Display d, Buffer b)
{
  if (debug)
    {
      assert (d != NULL);
      buffer_check (d->buffer);
      boolean_check (d->has_mode_line);
      assert (0 < d->width && 0 < d->height);
      assert (d->buffer == b);
    }
}

static void
display_ck (Display d)
{
  display_check (d, d->buffer);
}


Display
display_make (Buffer b, int width, int height, boolean has_mode_line)
{
  Display d = allot (sizeof *d);

  d->buffer = b;
  d->width = width;
  d->height = height;
  d->has_mode_line = has_mode_line;

  display_ck (d);
  return d;
}


void
display_unmake (Display d)
{
  display_ck (d);
  unallot (d);
}


Buffer
display_buffer (Display d)
{
  display_ck (d);
  return d->buffer;
}


Coord
display_start (Display d)
{
  display_ck (d);
  return buffer_page (d->buffer);
}


int
display_width (Display d)
{
  display_ck (d);
  return d->width;
}

int
display_height (Display d)
{
  display_ck (d);
  return d->height;
}


void
display_set_width (Display d, int width)
{
  display_ck (d);
  assert (0 <= width);
  d->width = width;
  display_ck (d);
}

void
display_set_height (Display d, int height)
{
  display_ck (d);
  assert (0 <= height);
  d->height = height;
  display_ck (d);
}


static void
put_repeated (Terminal term, int n, char c)
{
  for (; 0 < n; --n)
    terminal_put_glyph (term, c);
}


static int
put_string (Terminal term, const char *string, int width)
{
  for (; 0 < width && *string != '\0'; ++string, --width)
    terminal_put_glyph (term, *string);
  return width;
}


static void
put_mode_line (Terminal term, Buffer b, int columns)
{
  const char *filename = buffer_filename (b);
  put_repeated (term, 3, '-');
  columns -= 3;
  if (filename)
    columns = put_string (term, filename, columns);
  put_repeated (term, columns, '-');
}


static boolean
try_update (Display d, Terminal term, int left, int top)
{
  Style style = buffer_style (d->buffer);
  Text t = buffer_text (d->buffer);
  Coord point = buffer_point (d->buffer);
  int right = left + d->width;
  int bottom = top + d->height;
  
  Coord scan = buffer_page (d->buffer);
  Coord end = text_end (t);

  int point_row = -1, point_col = -1;

  terminal_normal_attribute (term);
  terminal_move_to (term, left, top);

  for (;;)
    {
      if (scan == point && term->y < bottom)
	point_row = term->y, point_col = term->x;

      if (term->y == bottom || scan == end)
	break;
      else
	{
	  Char c;
	  text_get (t, scan++, 1, &c);
	  if (c == '\n')
	    terminal_newline (term, right, left);
	  else 
	    {
	      Glyph glyphs [8];	/* FIXME: ensure size >= style->tab_width */
	      int n = style_format (style, c, term->x, glyphs);  // XXX shouldn't it be x - left instead of x?
	      int i = 0;

	      if (right <= term->x + n)
		{		/* break it with a line-continuation glyph */
		  for (; term->x < right - 1; ++i)
		    terminal_put_glyph (term, glyphs [i]);
		  terminal_put_glyph (term, '\\');
		  terminal_move_to (term, left, term->y + 1);
		  /* Now we assume the rest will fit on the next row.
		     FIXME: That assumption breaks if d->width < 4. */
		}
	      if (term->y < bottom)
		for (; i < n; ++i)
		  terminal_put_glyph (term, glyphs [i]);
	      if (right <= term->x)
		terminal_move_to (term, left, term->y + 1);
	    }
	}
    }

  /* Fail if the cursor turned out to be off-screen. */  
  if (point_row == -1)
    return no;

  /* We've reached the end of either the text or the window.  Fill in
     the rest of the window with blanks. */
  while (term->y < bottom)
    terminal_newline (term, right, left);

  /* Show the mode line. */
  if (d->has_mode_line)
    {
      terminal_reverse_attribute (term);
      put_mode_line (term, d->buffer, d->width);
      terminal_normal_attribute (term);
    }

  /* Now place the cursor on the screen. */
  terminal_move_to (term, point_col, point_row);
  return yes;
}


/* Update d's portion of the screen, with (x,y) as its upper-left corner. */
void
display_update (Display d, Terminal term, int x, int y)
{
  display_ck (d);

  if (try_update (d, term, x, y))
    return;

  /* The cursor was off-screen, so try again recentered: */
  display_center (d);
  if (try_update (d, term, x, y))
    return;

  unreachable;
}


void
display_scroll_to (Display d, Coord start)
{
  display_ck (d);
  buffer_set_page (d->buffer, start);
  display_ck (d);
}


/* Pre: `p' is the start of a row in `d'.
   Return the start of the next row (or the first one after the 
   visible display, or just `p' if there is no next row). */
Coord
display_next_row (Display d, Coord p)
{
  display_ck (d);
  {
    Style style = buffer_style (d->buffer);
    Text t = buffer_text (d->buffer);
    Coord end = text_end (t);
    int width = d->width;

    int col = 0, i = 0;
    while (p + i < end)
      {
	Char c;
	text_get (t, p + i++, 1, &c);
	if (c == '\n') return p + i;
	col += style_format (style, c, col, style_dev_null);
	if (width <= col) return p + i;
      }

    display_ck (d);
    return p;
  }
}


/* FIXME: this precondition is a bit too tight: */

/* Pre: `p' is the start of a row in `d' and is > 0.
   Return the start of the previous row (or the first one before the
   visible display.). */
Coord
display_previous_row (Display d, Coord p)
{
  Text t = buffer_text (d->buffer);
  Coord row_start = text_beginning_of_line (t, p);
  if (row_start == p)
    row_start = text_beginning_of_line (t, p - 1);

  for (;;)
    {
      Coord next = display_next_row (d, row_start);
      if (p <= next)
	return row_start;
      row_start = next;
    }

  display_ck (d);
}


/* Pre: `p' is a visible position in `d'.
   Return the start of its row. */
Coord
display_beginning_of_row (Display d, Coord p)
{
  Text t = buffer_text (d->buffer);
  Coord row_start = max (buffer_page (d->buffer),
			 text_beginning_of_line (t, p));

  for (;;)
    {
      Coord next = display_next_row (d, row_start);
      if (p < next)
	return row_start;
      if (p == next)
	return p;
      if (row_start == next)
	return row_start;	/* if there is no next row */
      row_start = next;
    }

  display_ck (d);
}


/* Change d's start point so that its buffer's `point' is in the
   middle line of the window, if possible; in any case ensure that the
   point is visible. */
void
display_center (Display d)
{
  display_ck (d);

  /* the following line is a horrible hack: */
  display_scroll_to (d, text_beginning_of_line (buffer_text (d->buffer),
						buffer_point (d->buffer)));
  {
    Coord r = display_beginning_of_row (d, buffer_point (d->buffer));
    display_scroll_to (d, display_nth_previous_row (d, r, d->height / 2));
  }

  display_ck (d);
}
