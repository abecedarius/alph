/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#include "doe.h"
#include <string.h>

/* This is all practically identical to the Mark_list code in text.c */

struct Name_table {
  int count, limit;
  struct Pair {
    const Char *name;
    Buffer buffer;
  } *array;
};


void
name_table_check (Name_table nt)
{
  if (debug)
    {
      assert (nt != NULL);
      assert (0 <= nt->count && nt->count <= nt->limit);
      assert (nt->limit == 0 || nt->array != NULL);
      {
	int i;
	for (i = nt->count - 1; 0 <= i; --i)
	  {
	    assert (nt->array [i].name != NULL);
	    buffer_check (nt->array [i].buffer);
	  }
      }
    }
}


Name_table
name_table_make (void)
{
  Name_table nt = allot (sizeof *nt);

  nt->count = 0;
  nt->limit = 0;
  nt->array = NULL;
  
  name_table_check (nt);
  return nt;
}


static void
destroy_all_buffers (Name_table nt)
{
  int i, n = name_table_count (nt);
  for (i = n - 1; 0 <= i; --i)
    {
      const Char *key = name_table_nth_key (nt, i);
      Buffer b = name_table_ref (nt, key);
      Text t = buffer_text (b);
      Style s = buffer_style (b);
      name_table_remove (nt, key);
      buffer_unmake (b);
      text_unmake (t);
      style_unmake (s);
    }
}


void
name_table_unmake (Name_table nt)
{
  name_table_check (nt);
  destroy_all_buffers (nt);	/* ugh */
  unallot (nt->array);
  unallot (nt);
}


/* TODO: decide what happens when name is already in the table */
void
name_table_add (Name_table nt, const Char *name, Buffer b)
{
  int limit = nt->limit;
  name_table_check (nt);
  
  /* Expand the array if it's too small: */
  if (nt->count == limit)
    {
      limit += limit/2 + 4;	/* FIXME: handle overflow */
      nt->array = reallot (nt->array, limit * sizeof nt->array[0]);
      nt->limit = limit;
    }

  nt->array [nt->count].name = name;
  nt->array [nt->count].buffer = b;
  nt->count++;

  name_table_check (nt);
}


Buffer
name_table_ref (Name_table nt, const Char *name)
{
  int i;
  name_table_check (nt);

  for (i = nt->count - 1; 0 <= i; --i)
    if (0 == strcmp (nt->array [i].name, name))
      return nt->array [i].buffer;
  return NULL;
}


void
name_table_remove (Name_table nt, const Char *name)
{
  int i;
  name_table_check (nt);

  for (i = nt->count - 1; 0 <= i; --i)
    if (0 == strcmp(nt->array [i].name, name))
      {
	memmove (nt->array + i,
		 nt->array + i + 1,
		 sizeof (nt->array [0]) * (nt->count - i - 1));
	--nt->count;
	name_table_check (nt);
	return;
      }

  unreachable;
}


int
name_table_count (Name_table nt)
{
  name_table_check (nt);
  return nt->count;
}


const Char *
name_table_nth_key (Name_table nt, int n)
{
  name_table_check (nt);
  assert (0 <= n && n < nt->count);
  return nt->array [n].name;
}
