/* Copyright 2000-2005 by Darius Bacon; see the file COPYING. */

#ifndef DOE_H
#define DOE_H

/* #include "memwatch.h" */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

enum { debug = 1 };		/* set to 1 for debugging mode */


/* Basic helpers */

void error (const char *format, ...);
void stdlib_error (const char *context);

void *allot_fn   (const char *file, int line, size_t size);
void *reallot_fn (const char *file, int line, void *ptr, size_t new_size);
void unallot_fn  (const char *file, int line, void *ptr);

#define allot(size)          allot_fn(__FILE__, __LINE__, (size))
#define reallot(ptr, size)   reallot_fn(__FILE__, __LINE__, (ptr), (size))
#define unallot(ptr)         unallot_fn(__FILE__, __LINE__, (ptr))

int max (int x, int y);
int min (int x, int y);

void copy_file (const char *dest_filename, FILE *dest, 
		const char *src_filename, FILE *src);
void backup_file (const char *filename);

typedef enum { no = 0, yes = 1 } boolean;

void boolean_check (boolean b);

#define unreachable   assert(no)


/* Characters */

typedef char Char;		/* what is stored internally */
typedef const Char *const_Char_ptr;

typedef char Glyph;		/* what may appear on the screen */


/* Sets of characters */

typedef struct {
  unsigned u0, u1, u2, u3;
} Char_set;

extern const Char_set char_set_empty;

Char_set  char_set_range       (Char lo, Char hi);

Char_set  char_set_not         (Char_set);
Char_set  char_set_and         (Char_set, Char_set);
Char_set  char_set_or          (Char_set, Char_set);

/* TODO: use pointer argument for efficiency (?) */
boolean   char_set_has         (Char_set, Char);


/* Text
   A text represents a sequence of characters along with a set of
   `marks', positions in the sequence that stick to their surrounding
   characters even as the sequence is mutated.  Coordinates are
   automatically clipped to within the sequence.

   The implementation makes insertions and deletions anywhere in the
   sequence usually efficient as long as those changes are usually
   spatially clustered.  */

typedef int Coord;
enum { nowhere = -1 };

typedef enum { backward = 0, forward = 1 } Direction;

void      direction_check      (Direction d);

typedef struct Mark *Mark;
typedef struct Text *Text;

Text      text_make            (int size);
void      text_check           (Text t);
void      text_unmake          (Text t);

Coord     text_end             (Text t);

/* TODO: replace with FSM-specified searching */
Coord     text_find_char_set   (Text t, Coord p, Direction d, Char_set cs);

/* FIXME: need to return the actual length gotten */
void      text_get             (Text t, Coord start, int length, Char *dest);
void      text_replace         (Text t, Coord start, int length,
				const Char *src, int src_size);

Mark      mark_make            (Text t, Coord p, Direction d);
void      mark_check           (Mark m, Text t);
void      mark_unmake          (Mark m);

Text      mark_text            (Mark m);
Coord     mark_coord           (Mark m);
Direction mark_direction       (Mark m);


/* Style
   This decides how lines will appear on a display. */

typedef struct Style *Style;

Style     style_make           (int tab_width);
void      style_check          (Style s);
void      style_unmake         (Style s);

extern Glyph style_dev_null[]; /* for style_format() to ignore its output */

int       style_format         (Style s, Char c, int column, Glyph *array);

int       style_column         (Style s, Text t, Coord p);
Coord     style_forward_columns (Style s, Text t, Coord p, int columns);


/* Command attributes */

typedef enum {
  attr_kill        = 1 << 0,
  attr_goal_column = 1 << 1
} Command_attributes;


/* Buffer
   Represents text with a current insertion point and a bunch of extra
   state variables, too.  It's basically the state of one file-like thing
   inside the editor. */

typedef struct Buffer *Buffer;

Buffer    buffer_make          (Text text, Style s, const char *filename);
void      buffer_check         (Buffer b);
void      buffer_unmake        (Buffer b);

Coord     buffer_point         (Buffer b);
void      buffer_move_to       (Buffer b, Coord p);

Coord     buffer_mark          (Buffer b);
void      buffer_set_mark      (Buffer b, Coord p);

Coord     buffer_page          (Buffer b);
void      buffer_set_page      (Buffer b, Coord p);

Text      buffer_text          (Buffer b);
Style     buffer_style         (Buffer b);
const char *buffer_filename    (Buffer b);

Command_attributes buffer_previous_command (Buffer b);
void      buffer_set_previous_command (Buffer b, Command_attributes previous);

int       buffer_goal_column   (Buffer b);
void      buffer_set_goal_column (Buffer b, int column);


/* Name table
   Lists all the displays active in an editor, by name. */

typedef struct Name_table *Name_table;

Name_table name_table_make     (void);
void      name_table_check     (Name_table nt);
void      name_table_unmake    (Name_table nt);

void      name_table_add       (Name_table nt, const Char *name, Buffer b);
void      name_table_remove    (Name_table nt, const Char *name);
Buffer    name_table_ref       (Name_table nt, const Char *name);

int       name_table_count     (Name_table nt);
const Char *name_table_nth_key (Name_table nt, int n);


/* Terminal
   A device with a source of input keystrokes and a screen showing a
   2d array of glyphs.  (Eventually we'll have to handle resize events 
   and polling for keystrokes, too.) */

typedef struct Terminal {
  int x, y;			/* consider these read-only */
} *Terminal;

Terminal  terminal_make        (void);
void      terminal_check       (Terminal t);
void      terminal_unmake      (Terminal t);

void      terminal_get_size    (int *x, int *y, Terminal t);

void      terminal_refresh     (Terminal t);
void      terminal_move_to     (Terminal t, int x, int y);
void      terminal_put_glyph   (Terminal t, Glyph c);
void      terminal_newline     (Terminal t, int right, int left);
void      terminal_normal_attribute (Terminal t);
void      terminal_reverse_attribute (Terminal t);

int       terminal_get_key     (Terminal t);

#define CTRL(c)  (~64 & (c))
#define META(c)  (128 | (c))


/* Display
   A window that interactively shows what's in a buffer. */

typedef struct Display *Display;

Display   display_make         (Buffer b,
				int width, int height, 
				boolean has_mode_line);
void      display_check        (Display d, Buffer b);
void      display_unmake       (Display d);

Buffer    display_buffer       (Display d);
Coord     display_start        (Display d);
int       display_width        (Display d);
int       display_height       (Display d);
void      display_set_width    (Display d, int width);
void      display_set_height   (Display d, int height);

void      display_update       (Display d, Terminal term, int left, int top);
void      display_center       (Display d);
void      display_scroll_to    (Display d, Coord start);

Coord     display_next_row     (Display d, Coord p);
Coord     display_previous_row (Display d, Coord p);
Coord     display_beginning_of_row (Display d, Coord p);


/* Frame
   A group of displays, one of them active, one of them a minibuffer. */

typedef struct Frame *Frame;

Frame     frame_make           (Terminal term, Name_table nt);
void      frame_check          (Frame f);
void      frame_unmake         (Frame f);

Terminal  frame_terminal       (Frame f);
int       frame_width          (Frame f);
int       frame_height         (Frame f);

Display   frame_split          (Frame f, Buffer b);

Display   frame_active_display (Frame f);
void      frame_set_active_display (Frame f, Display d);

Display   frame_minibuffer     (Frame f);
void      frame_set_message    (Frame f, const char *message);

int       frame_display_count  (Frame f);
Display   frame_nth_display    (Frame f, int n);

void      frame_update         (Frame f);

/* FIXME: temporary (hopefully) */
void      frame_replace_active_display (Frame f, Buffer b);


/* Interaction */

void      interactor_loop      (Frame f);
void      warn                 (const char *message);

/* For convenience we pass in d, b, and t even though you can get them
   out of f. */
typedef void (*Command) (int key, Frame f, Display d, Buffer b, Text t);

typedef const struct {
  int key;
  Command_attributes attributes;
  Command command;
} Binding, **Key_map;

extern Binding *doe_default_root_keymap[];

enum { any_key = -1 };

#define DEFCMD(name) \
  static void cmd_##name(int key, Frame f, Display d, Buffer b, Text t)

void cmd_keep_looking (int key, Frame f, Display d, Buffer b, Text t);

Key_map key_map_extend (Key_map km, Binding *bindings);


/* Editor */

typedef struct Editor *Editor;

Editor    editor_make          (Terminal term);
void      editor_check         (Editor e);
void      editor_unmake        (Editor e);

Name_table editor_name_table   (Editor e);
Frame     editor_frame         (Editor e);
Text      editor_killed        (Editor e);

Key_map   editor_fundamental_keymap (Editor);
void      editor_set_fundamental_keymap (Editor, Key_map);

extern Editor the_editor;


/* Derived functions */

void      derived_setup        (void);
void      derived_teardown     (void);

Char_set  char_set_from_string (const char *string);

void      text_copy            (Text dest, Coord at, 
				Text src, Coord p, int length);

void      text_delete_span     (Text t, Coord p, int length);
void      text_kill            (Text killed, Direction d, 
				Text t, Coord p, int length);

Coord     text_beginning_of_line (Text t, Coord p);
Coord     text_end_of_line     (Text t, Coord p);

Coord     text_backward_word   (Text b, Coord p);
Coord     text_forward_word    (Text b, Coord p);

void      text_insert_char     (Text t, Coord p, Char c);

void      text_insert_file     (Text t, Coord p, const char *filename);
void      text_write_file      (Text t, Coord p, int length, 
				const char *filename);

Coord     text_beginning_of_paragraph (Text t, Coord p);
void      text_fill_paragraph  (Text t, Coord p, int column, int width);

void      buffer_move          (Buffer b, int offset);

void      buffer_backward_word (Buffer b);
void      buffer_forward_word  (Buffer b);

void      buffer_next_line     (Buffer b);
void      buffer_previous_line (Buffer b);

void      buffer_backward_kill_word (Buffer b);
void      buffer_kill_word     (Buffer b);
void      buffer_kill_line     (Buffer b);
void      buffer_kill_region   (Buffer b);
void      buffer_kill_ring_save (Buffer b);
void      buffer_yank          (Buffer b);

void      buffer_insert_char   (Buffer b, Char c);
void      buffer_delete_span   (Buffer b, int count);

void      buffer_transpose_chars (Buffer b);

void      buffer_downcase_word (Buffer b);
void      buffer_upcase_word   (Buffer b);

void      buffer_beginning_of_line (Buffer b);
void      buffer_end_of_line   (Buffer b);

void      buffer_loudly_set_mark (Buffer b);
void      buffer_swap_point_and_mark (Buffer b);

void      buffer_save_file     (Buffer b);

boolean   buffer_search        (Buffer b, Coord p, Text target, Coord length);

void      buffer_fill_paragraph (Buffer b, int width);

Coord     display_find_row     (Display d, int row);
Coord     display_nth_previous_row (Display d, Coord start, int n);

void      display_scroll_up    (Display d);
void      display_scroll_down  (Display d);
void      display_scroll_to_end (Display d);


/* Main entry point */

void      doe_setup            (void);
void      doe_teardown         (void);

void      doe_visit            (const char *filename);
void      doe_interact         (void);


#endif
