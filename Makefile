CC     := gcc
CFLAGS := -Wall -g2 -O2 -std=c99 -pedantic
LDFLAGS :=

# CFLAGS := -g -O2 -Wall -ansi -pedantic
# CFLAGS := -g2 -Wall -ansi -pedantic
# ifeq ($(CC),gcc)
# 	CFLAGS += -Wall -W -Wpointer-arith -Wstrict-prototypes
# endif

all: og/runog tusl/runtusl semnet/semnet curst/run

# See http://www.pcug.org.au/~millerp/rmch/recu-make-cons-harm.html
# for an explanation of this makefile structure.

# Each module will add to these.
CLEAN  := 
OFILES := 

# The description for each module.
include tusl/Module.mk
include doe/Module.mk
include alph/Module.mk
include og/Module.mk
include semnet/Module.mk
include curst/Module.mk

clean:
	rm -f $(CLEAN) $(OFILES) *.exe *.stackdump */*.exe
